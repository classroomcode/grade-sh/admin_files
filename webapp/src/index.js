import * as ReactDOM from "react-dom"
import * as React from "react"
import App from "./app/app"


(new Promise((resolve, failure) =>
{
    let sock =new WebSocket(
        `ws://localhost:${service_port}`
    )
    sock.onopen = () => resolve(sock)
    sock.onerror = () => failure()
    
})).then(
    (sock) => 
    {
        window.sock = sock
        window.sock.local = true;
        ReactDOM.render(<App />, 
            document.getElementById("root")
        );
    }
).catch(
    () =>
    {
        window.sock = new WebSocket(
            `ws://${window.onion_link}`
        )

        window.sock.onerror = () => 
        {
            ReactDOM.render(<div>Service not running :(</div>, 
                document.getElementById("root")
            );
        }

        window.sock.onopen = () =>
        {
            ReactDOM.render(<App />,
                document.getElementById("root")
            );
        }
    }
)
