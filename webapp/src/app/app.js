import * as React from "react"
import "./app.css"

const ALL=0
const STDIO_TEST=1
const TYPECHECK_TEST=2
const FORMAT_TEST=3
const ARG_TEST=4    // Not yet implemented
const UNIT_TEST=5   // Not yet implemented
const CFG_TEST=6    // Not yet implemented
const STDIO_HTML = ["_test.txt", ".html"]

export default
class App extends React.Component {
    
    render()
    {
        return <div className="App">
            <div>
                {this.render_stdio()}                
            </div>
        </div>
    }

    render_stdio()
    {
        
        let children = Object.keys(this.stdio_tests).map(
            (key, index) =>
            {
                let [inputfile, score, diff, waiting] = this.stdio_tests[key];
                let onTest = () => 
                {   
                    this.stdio_tests[inputfile] = [inputfile,score,diff,true]
                    
                    this.setState({...this.state,
                            stdio_tests:this.state.stdio_tests,
                            disabled: true});
                    this.send(["test", "-c", inputfile]);
                }

                let onDiff = () =>
                {
                    let stdio_tests = {...this.stdio_tests}
                    for (let k of Object.keys(stdio_tests)) {
                        // toggle off all diffs
                        stdio_tests[k][2] = false;
                    }
                    // toggle on this diff
                    stdio_tests[inputfile][2] = true;
                    this.setState({...this.state, stdio_tests});
                }

                let onDiffRight = (event) =>
                {
                    // Show alt diff
                    event.preventDefault();
                    if (window.sock.local) {
                        this.send({
                            "method": "diff",
                            "params": [STDIO_TEST, inputfile],
                        }, false)
                    }
                }
                
                return <StdioTest
                    show_command={this.show_command}
                    waiting={waiting}
                    score={score}
                    key={index}
                    onTest={onTest}
                    onDiff={onDiff}
                    onDiffRight={onDiffRight}
                    diff={this.state.stdio_tests[inputfile][2]}
                    disabled={this.state.disabled}
                >{inputfile}</StdioTest>
            }
        )
        
        if (! children) {
            return null;
        }

        return (
            <div>
                <Separator href="./stdio_tests">stdio tests</Separator>
                {children}
            </div>)
    }
    constructor(props)
    {
        super(props)
        this.state = {
            stdio_tests: {},
            arg_tests: {},
            unittest_tests:{},
            cfg_tests:{},
            
            format_score: 0,
            type_score: 0,
            total: 0,
            show_command: true,
            disabled: false,
        }
        
        window.sock.onmessage = (message) =>
        {
            
            let [type, msg, score] = JSON.parse(message.data);
            console.log(type, msg, score)
            switch (type) {
                case ALL:
                    this.total = score;
                    break
                case STDIO_TEST:    
                    let diff = false;    
                    try {
                        diff = this.state.stdio_tests[msg][2]
                    }
                    catch {}

                    this.update_stdio([msg, score, diff, false]);
                    break
                case null:
                    this.setState({...this.state, disabled:false});
            }
        }

        this.send = (params, disabled=true) => 
        {
            this.setState({...this.state, disabled});
            window.sock.send(JSON.stringify(params))
        }

        window.sock.send(JSON.stringify(["lsof"]))
    }

    get show_command()
    {
        return this.state.show_command
    }

    set show_command(show_command)
    {
        this.setState({...this.state, show_command})
    }

    get stdio_tests()
    {
        return this.state.stdio_tests
    }
    
    update_stdio(values)
    {
        let stdio_test = this.state.stdio_tests
        stdio_test[values[0]] = values;
        this.setState({...this.state, stdio_test})
    }

    get format_score()
    {
        return this.state.format_score
    }
    
    set format_score(format_score)
    {
        this.setState({...this.state, format_score})
    }

    get type_score()
    {
        return this.state.type_score;
    }

    set type_score(type_score)
    {
        this.setState({...this.state, type_score});
    }
}

function
Separator(props)
{
    return <div className="Separator">
        <a href={props.href} target="_blank">{props.children}</a>
    </div>
}


function
StdioTest(props) // props: show_command, onTest, onDiff, waiting, score
{
    let show_command = props.show_command ? "default" : "none";
    let diffname = props.children.replace(STDIO_HTML[0], STDIO_HTML[1]);
    let link = `./stdio_tests/inputs/${props.children}`
    return <div className="StdioTest">
        <div style="CommandText" style={{display: show_command}}>
            {props.command}
        </div>
        <div>
            <button className="Test"
                    onClick={props.onTest}
                    disabled={props.disabled}
            >
                Test
            </button>
            <button className="Diff" onClick={props.onDiff} onContextMenu={
                props.onDiffRight}>Diff</button>
            <a className="InputDiv" href={link} target="_blank">{props.children}</a>
            <div className="ScoreDiv">{
                props.waiting ? "..." : props.score
            }</div>
        </div>
        {DiffFrame.Render(`./stdio_tests/diffs/${diffname}`, props.diff)}
    </div>
}

class DiffFrame extends React.Component {

    constructor(props)
    {
        super(props)
        this.state = {
            
        }
        this.ref = React.createRef()
    }

    
    render()
    {
        if (this.props.diff) {
            return <iframe src={this.props.path}
                style={{height: this.state.height, overflow: "auto"}}
                ref={this.ref}
                className="DiffFrame"
                height={this.state.height}/>
        }
        
        return null;
    }

    static Render(path, diff)
    {
        if (! diff) {
            return null;
        }
        return <DiffFrame path={path} diff={diff}/>
    }
};