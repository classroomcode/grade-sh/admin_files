database.start()
{
    coproc TESTDATA (
            python3 .admin_files/testdata.py
    ) 2>&1

    trap "echo 'quit' >&${TESTDATA[1]} && kill -15 $TESTDATA_PID" EXIT

    for testdef in .admin_files/tester/*_tests.sh; do
        source $testdef
    done

    for testclass in *_tests; do
        if command -v ${testclass}.${language}.register > /dev/null; then
            ${testclass}.${language}.register
        elif command -v ${testclass}.register > /dev/null; then
            ${testclass}.register
        fi
    done

    if [ ! -z "$format_command" ]; then
        source .admin_files/tester/format_test.sh
        if command -v format_test.register >/dev/null; then
            format_test.register
        fi
    fi
    
    if [ ! -z "$static_analysis_command" ]; then
        source .admin_files/tester/static_test.sh
        if command -v static_test.register >/dev/null; then
            static_test.register
        fi
    fi
}

database.register()
{
    echo "register $1 $2 '$3'" >&${TESTDATA[1]}
}

database.get_test_command()
{
    local -n ref=$1; shift
    echo "command $@" >& ${TESTDATA[1]} && read -a ref <& ${TESTDATA[0]}
}

database.update()
{
    echo "update $@" >& ${TESTDATA[1]}
}

database.render()
{

    # this command fails on git-lab ci/cd
    if ! stty size >/dev/null 2>/dev/null; then
        echo "render $@" >& ${TESTDATA[1]}
    else
        echo "render $@ --width=$(stty size | awk '{print $2}')" >& ${TESTDATA[1]}
    fi

    while true; do
        read -r line <&${TESTDATA[0]}
        if [ "$?" != 0 ] || [ "$line" = "<end transmission>" ]; then
            break
        fi
        echo "$line"
    done
}

database.lsof()
{
    echo "lsof $@" >& ${TESTDATA[1]};
}

database.capture_start()
{
    echo "capture $@" >& ${TESTDATA[1]} && read -a line <& ${TESTDATA[0]}
    SIGUSR1=${line[1]}
}

database.testattr()
{
    echo "testattr $@" >& ${TESTDATA[1]}
}

database.testclass()
{
    echo "testclass" >& ${TESTDATA[1]}
}

database.capture_stop()
{
    # Signal process to stop switch to 'shell mode'
    kill -$SIGUSR1 $TESTDATA_PID
    echo "" >& ${TESTDATA[1]}
}

database.grade_summary()
{
    echo "grade --summary" >& ${TESTDATA[1]}
    
    local lines=()
    IFS=":"
    while read line; do
        if [ "$line" = "<end transmission>" ]; then
            break
        fi
        lines+=("$line")
    done <& ${TESTDATA[0]}
    
    for line in ${lines[@]}; do
        echo -e $line
    done | column -t
    IFS=" "   
}

database.grade()
{
    local -n result=$1
    echo "grade" >& ${TESTDATA[1]} && read result <& ${TESTDATA[0]}
}
