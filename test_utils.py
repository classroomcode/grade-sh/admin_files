#!/usr/bin/python3
# -*- coding: utf-8 -*-

import ast
import textwrap

class AssertionTransformer(ast.NodeTransformer):
    """
    Transform all assertion statements into unittest.TestCase method calls.
    """

    def __init__(self, code):
        self.stack = []
        self.code = code
        
    @staticmethod
    def try_wrapper_prototype(lineno):
        return ast.parse(
            textwrap.dedent(
            f"""
            try:
                pass
            except AssertionError as e:
                __exceptions.append((0, e))
            else:
                __exceptions.append((1, {lineno}))
            """)
        ).body[0]

    @staticmethod
    def format_message(msg):
        if msg is not None:
            msg = ast.Constant(
                value=f"\033[3m\033[33m{msg.value}\033[0m"
            )
        return msg


    def visit_Assert(self, node):
        assert not self.stack
        self.stack.append((node.test, node.msg))
        replacement = self.visit(node.test)
        # child visitor need to pop from stack otherwise a noop.
        if self.stack:
            print(
                f"Unprocessed node '{type(self.stack[-1]).__name__}'." 
                "This is our fault."
            )
            # Exit with correct exit code incase we didn't implement something
            exit(self.code)
        
        try_block = self.try_wrapper_prototype(node.lineno)
        try_block.body[0] = ast.Expr(
                replacement,
                lineno=node.lineno,
                end_lineno=node.end_lineno,
                col_offset=node.col_offset,
                end_col_offset = node.end_col_offset,
        )
        return try_block

    compare_node_map = {
        ast.Eq: "assertEqual",
        ast.NotEq: "assertNotEqual",
        ast.Gt: "assertGreater",
        ast.GtE: "assertGreaterEqual",
        ast.Lt: "assertLess",
        ast.LtE: "assertLessEqual",
        ast.In: "assertIn",
        ast.NotIn: "assertNotIn",
        ast.Is: "assertIs",
        ast.IsNot: "assertIsNot"
    }

    def visit_Compare(self, node):
        if self.stack:
            info = self.stack.pop()
            if len(info) == 2:
                node, msg = info
            else:
                node, msg = info[0], ""
        else:
            # Noop unless node is child of assert
            return node
        
        msg = self.format_message(msg)
        if len(node.ops) > 1:
            # Happens when node looks like '1 < x < 2'
            # Transform node from
            # assert 1 < x < 2
            # ->
            # assertTrue(1 < x < 2)
            return ast.Call(
                func=ast.Name(
                    id="assertTrue",
                    ctx=ast.Load(),
                ),
                args=[node],
                keywords=[] if msg is None else [ast.keyword(arg="msg", value=msg)]
            )

        else:
            # Binary compare. Transform node from
            # assert a == b
            # ->
            # assertEqual(a, b)
            
            left = node.left
            op = node.ops[0]
            right = node.comparators[0]
    
            return ast.Call(
                func=ast.Name(
                    id=self.compare_node_map[type(op)], 
                    ctx=ast.Load(),
                ),
                args=[left, right],
                keywords=[] if msg is None else [ast.keyword(arg="msg", value=msg)],
            )

def main():
    import subprocess
    import sys
    import linecache
    from unittest.result import TestResult
    from unittest import TestCase
    from unittest import TestSuite
    from subprocess import DEVNULL
    from typing import List, Tuple, Union
    import traceback
    import os

    filename = sys.argv[1]
    code = int(sys.argv[2]); sys.argv[2] = "nice try..."

    # Evaluate assertion statements in a subprocess first to prevent global
    # injection attack. This also speeds up execution of passing tests since
    # we won't need to generate useful outputs
    assert os.path.exists("./unit_tests")
    sys.path.append(os.path.abspath("./unit_tests"))

    try:
        __import__(os.path.splitext(os.path.split(filename)[-1])[0])
    except BaseException as e:
        pass
    else:
        exit(code)
    finally:
        if os.path.exists("./unit_tests/__pycache__"):
            import shutil
            shutil.rmtree("./unit_tests/__pycache__")



    with open(filename, "r") as fp:
        source = fp.read()
    
    tree = AssertionTransformer(
        code
    ).visit(ast.parse(source))

    ast.fix_missing_locations(tree)
    #import astor; print(astor.to_source(tree))
    module = compile(tree, filename, "exec")
    collected_exceptions: List[Tuple[int, Union[Exception, int]]] = []

    class Tester(TestCase):
        def runTest(self):
            return exec(module, {
                "__exceptions":collected_exceptions,
                "assertFalse": self.assertFalse,
                "assertTrue": self.assertTrue,
                "assertEqual": self.assertEqual,
                "assertNotEqual": self.assertNotEqual,
                "assertLess": self.assertLess,
                "assertLessEqual": self.assertLessEqual,
                "assertGreater": self.assertGreater,
                "assertGreaterEqual": self.assertGreaterEqual,
                "assertIn": self.assertIn,
                "assertNotIn": self.assertNotIn,
                "assertIs": self.assertIs,
                "assertIsNot": self.assertIsNot,
            })

    result = TestResult(stream=sys.stdout)
    result = TestSuite((Tester(), )).run(result)

    for ok, exc in collected_exceptions:
        if not ok:
            assert isinstance(exc, Exception)
            exc_tb = exc.__traceback__
            exc_location = exc_tb.tb_frame.f_code.co_filename
            exc_lineno = exc_tb.tb_lineno
        
            print(f"\033[31mFAILED\033[0m - {exc_location}:{exc_lineno}")
            print(" > " + linecache.getline(exc_location, exc_lineno))
            print("".join(traceback.format_exception_only(AssertionError, exc)))
        else:
            assert isinstance(exc, int)
            print(f"\033[36mOK\033[0m - {filename}:{exc}")
            print(" > " + linecache.getline(filename, exc))
        
        print("-" * 40)
    exit(1)


if __name__ == "__main__":
    main()
 