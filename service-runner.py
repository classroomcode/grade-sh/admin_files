import websockets
import os
import logging
import subprocess
import asyncio
import json
import shlex
import threading

from typing import Set
from queue import Queue

asyncio.get_event_loop()
logger = logging.getLogger(__name__)

ALL=0
STDIO_TEST=1
TYPECHECK_TEST=2
FORMAT_TEST=3
ARG_TEST=4
UNIT_TEST=5
CFG_TEST=6
INPUT_STRIP = "_test.txt"

LOCKFILE = ".lock"

class GradeSH:

    def __init__(self, port, hostname=""):
        if os.path.exists(LOCKFILE):
            print("Service is running")
            exit(0)

        self.port = port
        self.loop = asyncio.new_event_loop()
        self.command_queue = Queue()
        self.server: websockets.WebSocketServerProtocol = None
        self.clients: Set[websockets.WebSocketClientProtocol] = set()
        self.grader: subprocess.Popen = None 
        self.hostname = hostname

    def __enter__(self):
        def start():
            asyncio.set_event_loop(self.loop)
            # Try to find a valid port
            while True:
                try:
                    self.server = self.loop.run_until_complete(websockets.serve(
                        self.handler,
                        "localhost",
                        self.port,
                    ))
                except OSError:
                    exit()
                else: 
                    break
            try:
                with open(LOCKFILE, "w") as fp:
                    # lock file is also a <script> source
                    fp.write(f"window.service_port={self.port}\n")
                    fp.write(f"window.onion_link='{self.hostname}'")
            except OSError:
                exit(1)
            self.loop.run_forever()

        def reader():
            while True:
                message = bytes(self.command_queue.get(), encoding="utf-8")
                os.write(self.grader.stdin.fileno(), message)
                print(message)

                results = [] 
                while True:
                    byt = str(
                        self.grader.stderr.readline(1000),
                        encoding="utf-8",
                    )
                    try:
                        result = json.loads(byt)
                    except:
                        print(f"error : {byt}")
                        continue
                    else:
                        print(result)
  
                    results.append(result)
                    if result == [None, None, None]:
                        break
                
                async def runner():
                    for client in self.clients:
                        for r in results:
                            print(json.dumps(r))
                            await client.send(json.dumps(r))
                
                asyncio.run_coroutine_threadsafe(runner(), self.loop)

        threading.Thread(target=start, daemon=True).start()
        threading.Thread(target=reader, daemon=True).start()
        
        self.grader = subprocess.Popen(["bash", "grade.sh", "-s"],
            stdin=subprocess.PIPE,
            stdout=subprocess.DEVNULL,
            stderr=subprocess.PIPE,
        )
        return self
  
    def __exit__(self, *args):
        if os.path.exists(LOCKFILE):
            os.remove(LOCKFILE)
        if self.grader is not None:
            self.grader.terminate()
            self.grader.wait()
        
    
    async def handler(self, ws, path):
        print("Client joined")
        self.clients.add(ws)
        try:
            async for message in ws:
                data = json.loads(message)
                if isinstance(data, list):
                    self.command_queue.put(shlex.join(json.loads(message)) + "\n")
                elif isinstance(data, dict):
                    method = data["method"]
                    params = data["params"]

                    if method == "diff":
                        if ("TERM_PROGRAM" in os.environ 
                            and os.environ["TERM_PROGRAM"] == "vscode"
                        ):
                            test = params[0]
                            inputfile = params[1]
                            filename = inputfile.replace(INPUT_STRIP, "")
                            if test == STDIO_TEST:
                                a = f"stdio_tests/goals/{filename}_output.txt"
                                b = f"stdio_tests/outputs/{filename}_output.txt"
                            subprocess.call(["code", "--diff", b, a])
 
        finally:
            self.clients.remove(ws)
            print("Client left")


if __name__ == "__main__":
    import argparse
    import time
    parser = argparse.ArgumentParser()
    parser.add_argument("port", type=int)
    args = parser.parse_args()

    if os.path.exists(".hidden/ws/hostname"):
        with open(".hidden/ws/hostname", "r") as fp:
            hostname = fp.read().strip()
    else:
        hostname = ""

    with GradeSH(args.port, hostname) as service:
        try:
            while True:
                try:
                    service.command_queue.put(input() + "\n")
                except EOFError:
                    time.sleep(1)
        except KeyboardInterrupt:
            exit(0)