# Compiles C source with gcc and C++ source with g++
CPP_SRC := $(wildcard *.cpp)
C_SRC := $(wildcard *.c)

CPP_OBJECTS := $(CPP_SRC:.cpp=.o)
C_OBJECTS := $(C_SRC:.c=.o)

# g++ links both C and C++ runtime libs
$(main_file): $(CPP_OBJECTS) $(C_OBJECTS)
	g++ $(INCLUDE) $^ -o $@ $(LD_FLAGS)

objects: $(CPP_OBJECTS) $(C_OBJECTS)

%.o: %.cpp
	g++ $(INCLUDE) -c $< -o $@ $(CPP_FLAGS)

%.o: %.c
	gcc $(INCLUDE) -c $< -o $@ $(CC_FLAGS)
