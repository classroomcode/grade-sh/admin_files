# grade-sh

`grade.sh` uses the existing CI (continuous integration) environment provided by GitLab
to run a set of automated tests each time a student pushes a new commit.  CI frameworks
are already designed to test code correctness and prevent regressions, so this approach
is easily adapted to the grading of programming assignments.

Because grading is done entirely in the cloud, `grade.sh`
does not require the instructor to download the entire set of student code and execute
(potentially malicious) student code on their own computer.

## Setup
To create an assignment, clone one of the language templates below
|Language|Repo|
|-|-|
|Python|https://gitlab.com/classroomcode/grade-sh/python-template|
|C/C++|https://gitlab.com/classroomcode/grade-sh/c-cpp-template|

`cd` into the directory and run `bash grade.sh`

