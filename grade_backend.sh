#!/bin/bash

print_help() {
    echo "Usage: grade.sh [OPTIONS]"
    echo ""
    echo "Options:"
    echo "  -s                  Start as stream service"
    echo "         --share      Create shareable link"
    echo "         --serve      Start websocket service"
    echo "         --clean      Force service cleanup"
    echo "  -h     --help       Show this message"
}


for python_package_name in Levenshtein; do
    if ! python3 -c "import $python_package_name"; then
        echo -e "\npip: Install $package_name using "$MAGENTA""pip3 install --upgrade $python_package_name --user""$RESET" before proceeding, or"
        echo "OpenSuse: Install $package_name using "$MAGENTA""sudo zypper install python3-$python_package_name""$RESET" before proceeding, or"
        echo "Debian: Install $package_name using "$MAGENTA""sudo apt install python3-$python_package_name""$RESET" before proceeding"
        echo -e "    If not found, try lowercase spelling), or"
        echo "Fedora: Install $package_name using "$MAGENTA""sudo dnf install python3-$python_package_name""$RESET" before proceeding."
        exit 1
    fi
done
if [ $language = "python" ]; then
    # Some OS's (OpenSuse) install pudb3 as pudb
    if command -v pudb3 >/dev/null 2>&1; then
        pudb3=pudb3
    else
        pudb3=pudb
    fi
    if ! command -v $pudb3 &>/dev/null; then
        echo "Install pudb3 before proceeding."
        echo "pip3 install pudb --upgrade --user"
        echo "sudo dnf install python3-pudb"
        echo "sudo apt install python3-pudb"
        echo "sudo zypper install python3-pudb"
        exit 1
    fi
    for package_name in mypy black py2cfg; do
        if ! command -v $package_name &>/dev/null; then
            echo Install $package_name before proceeding.
            echo "$MAGENTA""pip3 install --upgrade $package_name --user"
            echo "sudo dnf install $package_name"
            echo "sudo apt install $package_name"
            echo "sudo zypper install $package_name""$RESET"
            exit 1
        fi
    done
fi
if [ $language = "bash" ]; then
    if ! [ -x ./shfmt ]; then
        if ! command -v shfmt &>/dev/null; then
            echo "Install shfmt before proceeding:"
            echo "$MAGENTA""wget -O ~/bin/shfmt https://github.com/mvdan/sh/releases/download/v3.1.2/shfmt_v3.1.2_linux_amd64""$RESET"
            echo "$MAGENTA""chmod +x ~/bin/shfmt""$RESET"
            exit 1
        fi
    fi
    for package_name in shellcheck; do
        if ! command -v $package_name &>/dev/null; then
            echo Install $package_name before proceeding.
            echo "Use your Linux system's package manager (apt/zypper/dnf/etc)."
            exit 1
        fi
    done
fi
if [ $language = "cpp" ]; then
    for package_name in clang-format cmake make cppcheck gdb; do
        if ! command -v $package_name &>/dev/null; then
            echo Install $package_name before proceeding.
            echo "Use your Linux system's package manager (apt/zypper/dnf/etc)."
            exit 1
        fi
    done
fi
if [ $language = "rust" ]; then
    for package_name in rustc gdb; do # TODO rustfmt on containter is not great?
        if ! command -v $package_name &>/dev/null; then
            echo Install $package_name before proceeding.
            echo "Use your Linux system's package manager (apt/zypper/dnf/etc)."
            exit 1
        fi
        for package_name in shellcheck; do
            if ! command -v $package_name &>/dev/null; then
                echo Install $package_name before proceeding.
                exit 1
            fi
        done
    done
fi

if [ $language = "cpp" ]; then
    for package_name in clang-format cmake make cppcheck; do
        if ! command -v $package_name &>/dev/null; then
            echo Install $package_name before proceeding.
            exit 1
        fi
    done
fi




grade_update() {
    # Updates grade and prints, based on the return code of preceeding statement
    # Expects grade for each unit/test to be 0-100
    # Usage: grade_update test_name points_to_add expected_retcode
    return_status=$? # Checks the return status of last call (to diff)
    check_hashes
    echo -e "\nTest for: $1"
    if [ "$return_status" == "$3" ]; then
        if [ "$2" -lt 100 ]; then
            echo "$RED    This test gave you $2 more points$RESET"
        else
            echo "$GREEN    This test gave you $2 more points$RESET"
        fi
        ((grade = grade + "$2"))
    else
        echo "$RED    This test gave you 0 more points$RESET"
    fi
    ((num_tests = num_tests + 1))
}

unit_tests() {
    # Executes a directory of our python unit tests.
    # Assumes they don't neeed any standard input (write a custom line for that).
    # Usage: unit_tests
    if [ "$language" = "cpp" ]; then
        glob_expr="unit_tests/*.cpp"
        pushd .admin_files >/dev/null 2>&1
        mkdir -p build >/dev/null 2>&1
        pushd build >/dev/null 2>&1
        cmake .. -DCMAKE_BUILD_TYPE=Debug -DMAIN_FILE:STRING="$main_file" >/dev/null 2>&1
        make >/dev/null 2>&1
        popd >/dev/null 2>&1
        popd >/dev/null 2>&1
        check_hashes
    elif [ "$language" = "python" ]; then
        glob_expr="unit_tests/*.py"
    elif [ "$language" = "bash" ]; then
        glob_expr="unit_tests/*.sh"
    elif [ "$language" = "rust" ]; then
        glob_expr="unit_tests/*.rs"
        cargo build
    fi
    first="0"
    for testpath in $glob_expr; do
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we will run unit tests.""$RESET"
            [ $annoying_nodebug = 'd' ] && echo "See the ./unit_tests/ folder for more detail."
            first="1"
            keepgoing
        fi
        subsection
        expected_retcode=$(((RANDOM % 250) + 5))
        filename=$(basename "$testpath")
        if [ "$language" = "cpp" ]; then
            testname="$(basename "$filename" .cpp)"
            echo Running command: $ " $MAGENTA"timeout "$process_timeout" ./.admin_files/build/"$testname" "$expected_retcode""$RESET"
            timeout "$process_timeout" ./.admin_files/build/"$testname" "$expected_retcode"
        elif [ "$language" = "python" ]; then
            echo Running command: $ " $MAGENTA"timeout "$process_timeout" python3 unit_tests/"$filename" "$expected_retcode""$RESET"
            timeout "$process_timeout" python3 unit_tests/"$filename" "$expected_retcode"
        elif [ "$language" = "bash" ]; then
            echo Running command: $ " $MAGENTA"timeout "$process_timeout" bash unit_tests/"$filename" "$expected_retcode""$RESET"
            timeout "$process_timeout" bash unit_tests/"$filename" "$expected_retcode"
        elif [ "$language" = "rust" ]; then
            testname="$(basename "$filename" .rs)"
            echo Running command: $ " $MAGENTA"timeout "$process_timeout" ./target/debug/"$testname" "$expected_retcode""$RESET"
            timeout "$process_timeout" ./target/debug/"$testname" "$expected_retcode"
        fi
        if [ $? -eq "$expected_retcode" ]; then
            grade_update "$filename" 100 0
        else
            grade_update "$filename" 0 0
            # https://stackoverflow.com/questions/20010199/how-to-determine-if-a-process-runs-inside-lxc-docker#20012536
            if [ "$annoying_nodebug" = "g" ] || grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
                :
            else
                if [ "$language" = "cpp" ]; then
                    debug_cmd=(gdb '--eval-command="break main"' '--eval-command="run"' --args ./.admin_files/build/"$testname" 123)
                elif [ "$language" = "python" ]; then
                    debug_cmd=("$pudb3" "$testpath" 123)
                elif [ "$language" = "bash" ]; then
                    debug_cmd=(bash -x "$testpath" 123)
                    # TODO a real bash debugger like bashdb?
                elif [ "$language" = "rust" ]; then
                    debug_cmd=(gdb '--eval-command="break main"' '--eval-command="run"' --args ./target/debug/"$testname" 123)
                fi
                echo -e "\nWe will now launch the debugger as follows:"
                echo -e "$ $MAGENTA" "${debug_cmd[@]}" "$RESET"
                keepgoing
                "${debug_cmd[@]}"
            fi
        fi
        check_hashes
    done
}

stdio_tests() {
    # Tests a python script (first and only arg) against directory of std-in/out
    # Saves outputs and diffs
    # Usage: stdio_tests main_file.py

    # For C++, build the student's main()
    if [ "$language" = "cpp" ]; then
        prog_name=("./program" "$main_file_arguments")
        # g++ -Wall -Werror -Wpedantic -g -std=c++14 $1 -o "$prog_name"
        # TODO: Can this be any more discerning, in case of multiple main()s?
        g++ -Wall -Werror -Wpedantic -g -std=c++14 ./*.cpp -o "program"
    elif [ "$language" = "python" ]; then
        prog_name=(python3 "$1" "$main_file_arguments")
    elif [ "$language" = "bash" ]; then
        prog_name=(bash "$1" "$main_file_arguments")
    elif [ "$language" = "rust" ]; then
        prog_name=("./program" "$main_file_arguments")
        cargo build
        mv ./target/debug/program program
    fi

    rm -rf stdio_tests/outputs/*
    rm -rf stdio_tests/goals/.*.swp
    first="0"
    for testpath in stdio_tests/inputs/*.txt; do
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we will run Standard Input/Output (std io) tests.""$RESET"
            [ $annoying_nodebug = 'd' ] && echo "See the ./stdio_tests/ folder for more detail, including diffs."
            first="1"
            keepgoing
        fi
        filename=$(basename "$testpath")
        testname="${filename%_*}"
        subsection
        echo -e "Running command: $ $MAGENTA" timeout "$process_timeout" "${prog_name[@]}" "<$testpath" ">stdio_tests/outputs/$testname"_output.txt"$RESET"
        t0=$(date +%s.%N)
        timeout "$process_timeout" "${prog_name[@]}" <"$testpath" >stdio_tests/outputs/"$testname"_output.txt
        check_hashes
        echo -e "Your main driver program took the following duration of time to run on the above sample:"
        your_time=$(echo "print($(date +%s.%N) - $t0)" | python3)
        echo -e "    $your_time seconds"
        diff -y -W 200 stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >stdio_tests/diffs/"$testname".txt
        vim -d stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt \
            -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
            -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
            -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
            -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White' \
            -c TOhtml \
            -c "w! stdio_tests/diffs/$testname.html" \
            -c 'qa!' >/dev/null 2>&1
        if [ "$fuzzy_partial_credit" = false ]; then
            diff stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >/dev/null 2>&1
            grade_update "$testpath" 100 0
            # Just a useless placeholder here:
            fuzzy_diff=0
        else
            fuzzy_diff=$(python3 .admin_files/fuzzydiffer.py "stdio_tests/goals/$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt)
            grade_update "$testpath" "$fuzzy_diff" 0
        fi
        diff stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >/dev/null 2>&1
        if [ "$?" -eq 0 ] || [ "$fuzzy_diff" -eq 100 ]; then
            # bash's no-op is most clear positive logic here...
            :
        else
            if [ "$annoying_nodebug" = "g" ] || grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
                :
            else
                if [ "$language" = "cpp" ]; then
                    # http://mywiki.wooledge.org/BashFAQ/050
                    debug_cmd=(gdb '--eval-command="break main"' --args "${prog_name[@]}")
                elif [ "$language" = "python" ]; then
                    debug_cmd=("$pudb3" "$1" "$main_file_arguments")
                elif [ "$language" = "bash" ]; then
                    debug_cmd=(bash -x "$1" "$main_file_arguments")
                    # TODO a real bash debugger like bashdb?
                elif [ "$language" = "bash" ]; then
                    debug_cmd=(gdb '--eval-command="break main"' --args "${prog_name[@]}")
                elif [ "$language" = "rust" ]; then
                    debug_cmd=(gdb '--eval-command="break main"' --args "${prog_name[@]}")
                fi
                echo -e "\nWe will now run the following to show you your non-caputred output:\n\t$ $MAGENTA" "${prog_name[@]}" "<$testpath$RESET"
                keepgoing
                echo ">>>>your output>>>>"
                "${prog_name[@]}" <"$testpath"
                check_hashes
                echo "<<<<your output<<<<"
                echo -e "\nWe will now show you the differences between your caputred standard out and the goal."
                echo "Type$MAGENTA esc :qa!$RESET to leave Vim when you are done."
                keepgoing
                # Either meld or vim works, up to you!
                # meld --diff "stdio_tests/goals/$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt &
                vim -d "stdio_tests/goals/$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt \
                    -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
                    -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
                    -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
                    -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White'
                echo -e "\nWe will now launch the debugger as follows:"
                echo -e "$ $MAGENTA" "${debug_cmd[@]}" "$RESET"
                echo -e "while YOU copy the contents of $ $MAGENTA cat $testpath $RESET by hand"
                keepgoing
                "${debug_cmd[@]}"
                check_hashes
            fi
        fi
    done
    if [ "$language" = "cpp" ] || [ "$language" = "rust" ]; then
        rm -f "${prog_name[@]}"
    fi
}

arg_tests() {
    # Tests a python script (first and only arg) against directory of std-in/out
    # Saves outputs and diffs
    # Usage: arg_tests main_file.py

    # For C++, build the student's main()
    if [ "$language" = "cpp" ]; then
        prog_name=("./program")
        # g++ -Wall -Werror -Wpedantic -g -std=c++14 $1 -o "$prog_name"
        # TODO: Can this be any more discerning, in case of multiple main()s?
        g++ -Wall -Werror -Wpedantic -g -std=c++14 ./*.cpp -o "program"
    elif [ "$language" = "python" ]; then
        prog_name=(python3 "$1")
    elif [ "$language" = "bash" ]; then
        prog_name=(bash "$1")
    elif [ "$language" = "rust" ]; then
        prog_name=("./program")
        cargo build
        mv ./target/debug/program program
    fi

    rm -rf arg_tests/outputs/*
    rm -rf arg_tests/goals/.*.swp
    first="0"
    for testpath in arg_tests/args/*.txt; do
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we will run argument-based tests.""$RESET"
            [ $annoying_nodebug = 'd' ] && echo "See the ./arg_tests/ folder for more detail, including diffs."
            first="1"
            keepgoing
        fi
        filename=$(basename "$testpath")
        testname="${filename%_*}"
        read -ra testargs <"$testpath"
        subsection
        echo -e "Running command: $ $MAGENTA" timeout "$process_timeout" "${prog_name[@]}" "${testargs[@]}" "$RESET"
        t0=$(date +%s.%N)
        timeout "$process_timeout" "${prog_name[@]}" "${testargs[@]}"
        check_hashes
        echo -e "Your main driver program took the following duration of time to run on the above sample:"
        your_time=$(echo "print($(date +%s.%N) - $t0)" | python3)
        echo -e "    $your_time seconds"
        diff -y -W 200 arg_tests/goals/"$testname"_output.txt arg_tests/outputs/"$testname"_output.txt >arg_tests/diffs/"$testname".txt
        vim -d arg_tests/goals/"$testname"_output.txt arg_tests/outputs/"$testname"_output.txt \
            -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
            -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
            -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
            -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White' \
            -c TOhtml \
            -c "w! arg_tests/diffs/$testname.html" \
            -c 'qa!' >/dev/null 2>&1
        if [ "$fuzzy_partial_credit" = false ]; then
            diff arg_tests/goals/"$testname"_output.txt arg_tests/outputs/"$testname"_output.txt >/dev/null 2>&1
            grade_update "$testpath" 100 0
            # Just a useless placeholder here:
            fuzzy_diff=0
        else
            fuzzy_diff=$(python3 .admin_files/fuzzydiffer.py "arg_tests/goals/$testname"_output.txt arg_tests/outputs/"$testname"_output.txt)
            grade_update "$testpath" "$fuzzy_diff" 0
        fi
        diff stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >/dev/null 2>&1
        if [ "$?" -eq 0 ] || [ "$fuzzy_diff" -eq 100 ]; then
            # bash's no-op is most clear positive logic here...
            :
        else
            if [ "$annoying_nodebug" = "g" ] || grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
                :
            else
                if [ "$language" = "cpp" ]; then
                    # http://mywiki.wooledge.org/BashFAQ/050
                    debug_cmd=(gdb '--eval-command="break main"' --args "${prog_name[@]}" "${testargs[@]}")
                elif [ "$language" = "python" ]; then
                    debug_cmd=("$pudb3" "$1" "${testargs[@]}")
                elif [ "$language" = "bash" ]; then
                    debug_cmd=(bash -x "$1" "${testargs[@]}")
                    # TODO a real bash debugger like bashdb?
                elif [ "$language" = "rust" ]; then
                    debug_cmd=(gdb '--eval-command="break main"' --args "${prog_name[@]}" "${testargs[@]}")
                fi
                echo -e "\nWe will now show you the differences between your argument-based output and ours."
                echo "Type$MAGENTA esc :qa!$RESET to leave Vim when you are done."
                keepgoing
                # Either meld or vim works, up to you!
                # meld --diff "arg_tests/goals/$testname"_output.txt arg_tests/outputs/"$testname"_output.txt &
                vim -d "arg_tests/goals/$testname"_output.txt arg_tests/outputs/"$testname"_output.txt \
                    -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
                    -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
                    -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
                    -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White'
                echo -e "\nWe will now launch the debugger as follows:"
                echo -e "$ $MAGENTA" "${debug_cmd[@]}" "$RESET"
                keepgoing
                "${debug_cmd[@]}"
                check_hashes
            fi
        fi
    done
    if [ "$language" = "cpp" ] || [ "$language" = "rust" ]; then
        rm -f "${prog_name[@]}"
    fi
}

cfg_tests() {
    # Tests that student implemented py files for each cfg_tests/*.svg file
    # Usage: cfg_tests

    # For C++, build the student's main()
    if [ "$language" = "cpp" ]; then
        :
        # TODO which cfg generator to use?
    elif [ "$language" = "python" ]; then
        cfg_generator=py2cfg
    elif [ "$language" = "bash" ]; then
        :
        # TODO Probably no cfg generator exists, or will?
    fi

    rm -rf *.svg
    first="0"
    for testpath in cfg_tests/*.svg; do
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we will run control-flow-graph based tests.""$RESET"
            [ $annoying_nodebug = 'd' ] && echo "See the ./cfg_tests/ folder for more detail."
            first="1"
            keepgoing
        fi
        subsection
        echo "Running command: $ $MAGENTA $cfg_generator ${testpath:10:${#testpath}-18}.py$RESET"
        $cfg_generator "${testpath:10:${#testpath}-18}.py"

        if diff "${testpath:10:${#testpath}}" "$testpath" &>/dev/null; then
            grade_update "Your code CFG match with $testpath test" 100 0
        else
            grade_update "Your code CFG match with $testpath test" 0 0
            # https://stackoverflow.com/questions/20010199/how-to-determine-if-a-process-runs-inside-lxc-docker#20012536
            if [ "$annoying_nodebug" = "g" ] || grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
                :
            else
                if [ "$language" = "cpp" ]; then
                    :
                    # TODO
                elif [ "$language" = "python" ]; then
                    # TODO what if the file had args?
                    debug_cmd=("py2cfg" "${testpath:10:${#testpath}-18}.py" "--debug")
                elif [ "$language" = "bash" ]; then
                    :
                    # TODO
                elif [ "$language" = "rust" ]; then
                    :
                    # TODO
                fi
                echo -e "\nWe will now launch the debugger as follows:"
                echo -e "$ $MAGENTA" "${debug_cmd[@]}" "$RESET"
                keepgoing
                "${debug_cmd[@]}"
            fi
        fi
    done
}

doctest_tests() {
    # Tests that student implemented py files for each cfg_tests/*.svg file
    # Usage: cfg_tests

    # For C++, build the student's main()
    if [ "$language" = "cpp" ]; then
        :
        doctest_string="junk"
        # TODO are there any doctest options for cpp?
    elif [ "$language" = "python" ]; then
        doctest_string="doctest.testmod"
        doctest_command="python3 -m doctest -v $1"
    elif [ "$language" = "bash" ]; then
        :
        doctest_string="junk"
        # likely these exist for bash
    fi
    if grep "$doctest_string" "$1" &>/dev/null; then
        section
        echo "$ORANGE""Below here, we will run doctest based tests.""$RESET"
        [ $annoying_nodebug = 'd' ] && echo "See the function docstring in $1 itself for more detail."
        keepgoing

        echo "Running command: $ $MAGENTA $doctest_command $RESET"
        if $doctest_command &>/dev/null; then
            grade_update "Doctests in function docstring" 100 0
        else
            grade_update "Doctests in function docstring" 0 0
            # https://stackoverflow.com/questions/20010199/how-to-determine-if-a-process-runs-inside-lxc-docker#20012536
            if [ "$annoying_nodebug" = "g" ] || grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
                :
            else
                if [ "$language" = "cpp" ]; then
                    :
                    # TODO
                elif [ "$language" = "python" ]; then
                    $doctest_command
                    debug_cmd=("$pudb3" "$1" "$main_file_arguments")
                elif [ "$language" = "bash" ]; then
                    :
                    # TODO
                elif [ "$language" = "rust" ]; then
                    :
                    # TODO
                fi
                echo -e "\nWe will now launch the debugger as follows:"
                echo -e "$ $MAGENTA" "${debug_cmd[@]}" "$RESET"
                keepgoing
                "${debug_cmd[@]}"
            fi
        fi
    fi
}

files_exist() {
    # https://stackoverflow.com/questions/4069188/how-to-pass-an-associative-array-as-argument-to-a-function-in-bash
    # https://stackoverflow.com/questions/3112687/how-to-iterate-over-associative-arrays-in-bash
    local -n arr=$1
    first="0"
    for exist_file in "${!arr[@]}"; do
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we will run tests for existence of files and their type data:""$RESET"
            first="1"
            keepgoing
        fi
        subsection
        echo -e "Checking for the existence of '$exist_file' and its containing type of data:" "'${arr["$exist_file"]}.'"
        [ -f "$exist_file" ] && file "$exist_file" | grep "${arr["$exist_file"]}" >/dev/null 2>&1
        grade_update \""$exist_file\" with type containing \"${arr["$exist_file"]}\" existed" 100 0
    done
}

transmit() {
    echo "[$1,\"$2\",$3]" >&2
}

end_transmission() {
    echo "[null, null, null]" >&2
}

_stdio_do_test() {
    # For C++, build the student's main()
    if [ "$language" = "cpp" ]; then
        prog_name=("./program" "$main_file_arguments")
        # g++ -Wall -Werror -Wpedantic -g -std=c++14 $1 -o "$prog_name"
        # TODO: Can this be any more discerning, in case of multiple main()s?
        g++ -Wall -Werror -Wpedantic -g -std=c++14 ./*.cpp -o "${prog_name[0]}"
    elif [ "$language" = "python" ]; then
        prog_name=(python3 "$main_file" "$main_file_arguments")
    elif [ "$language" = "bash" ]; then
        prog_name=(bash "$main_file" "$main_file_arguments")
    fi

    local filename=$1
    local testpath=stdio_tests/inputs/$filename
    local testname="${filename%_*}"
    
    echo -e "We're running: \$$MAGENTA" timeout "$process_timeout" "${prog_name[@]}" "<$testpath" ">stdio_tests/outputs/$testname"_output.txt"$RESET"
    timeout "$process_timeout" "${prog_name[@]}" <"$testpath" >stdio_tests/outputs/"$testname"_output.txt
    check_hashes
    diff -y -W 200 stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >stdio_tests/diffs/"$testname".txt
    vim -d stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt \
        -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
        -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White' \
        -c TOhtml \
        -c "w! stdio_tests/diffs/$testname.html" \
        -c 'qa!' >/dev/null 2>&1

    if [ "$fuzzy_partial_credit" = false ]; then
        diff stdio_tests/goals/"$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt >/dev/null 2>&1
        if [ "$?" == 0 ]; then
            stdio_grade_map[$filename]=100
        fi
    else
        fuzzy_diff=$(python3 .admin_files/fuzzydiffer.py "stdio_tests/goals/$testname"_output.txt stdio_tests/outputs/"$testname"_output.txt)
        if [ "$?" == 0 ]; then
            stdio_grade_map["$filename"]=$fuzzy_diff
        fi
    fi

    # transmit the result
    _do_lsof -c $filename
}

_typecheck_do_test() {
    if [ "$language" = "cpp" ]; then
        echo "We're running $" "$MAGENTA cppcheck --enable=all --error-exitcode=1 --language=c++ ./*.cpp ./*.h ./*.hpp$RESET"
        cppcheck --enable=all --error-exitcode=1 --language=c++ ./*.cpp ./*.h ./*.hpp
    elif [ "$language" = "python" ]; then
        echo "We're running $" "$MAGENTA mypy --strict --disallow-any-explicit ./*.py$RESET"
        mypy --strict --disallow-any-explicit ./*.py
    elif [ "$language" = "bash" ]; then
        echo "We're running $" "$MAGENTA shellcheck --check-sourced --external-sources $main_file $RESET"
        shellcheck --check-sourced --external-sources "$main_file"
    fi
    if [ "$?" == 0 ]; then
        type_score=100;
    else
        type_score=0;
    fi
    _do_lsof -t
}

_format_do_test() {
    local result;
    if [ "$language" = "cpp" ]; then
        echo "We're running $" "$MAGENTA python3 .admin_files/run-clang-format.py -r --style=LLVM --exclude './.admin_files/build/*' .$RESET"
        python3 .admin_files/run-clang-format.py -r --style=LLVM --exclude './.admin_files/build/*' .
        # CI needed this script instead of local command?
        # clang-format --dry-run --Werror --style=Microsoft *.cpp *.h *.hpp
        result=$?
    elif [ "$language" = "python" ]; then
        echo "We're running $" "$MAGENTA black --check ./*.py$RESET"
        black --check ./*.py
        result=$?
    elif [ "$language" = "bash" ]; then
        echo "We're running $" "$MAGENTA shfmt -i 4 -d .$RESET"
        # https://www.arachnoid.com/linux/beautify_bash/
        # https://github.com/mvdan/sh#shfmt
        # Format dir with: ./go/bin/shfmt -i 4 -w .
        if [ -x ./shfmt ]; then
            ./shfmt -i 4 -d .
        elif command -v shfmt >/dev/null 2>&1; then
            shfmt -i 4 -d .
        else
            result=$?
            echo "Install shfmt to run the Bash format check!"
        fi
    fi

    if [ "$result" == 0 ]; then
        format_score=100;
    else
        format_score=0;
    fi

    _do_lsof -f  
}

_do_test() {
    local OPTIND arg
    local do_format=0
    local do_typecheck=0
    local do_stdio=0

    while getopts ":c:tf" arg; do
        case "${arg}" in 
            t) do_typecheck=1;;
            f) do_format=1;;
            c) do_stdio=$OPTARG;;
        esac
    done

    if [ $do_stdio == 0 ] && [ $do_typecheck == 0 ] && [ $do_format == 0 ]; then
        for key in ${!stdio_grade_map[@]}; do
            _stdio_do_test $key
        done
        _typecheck_do_test
        _format_do_test
    elif [[ -v "stdio_grade_map[$do_stdio]" ]]; then
        _stdio_do_test $do_stdio
    elif [ "$do_typecheck" == 1 ]; then
        _typecheck_do_test
    elif [ "$do_format" == 1 ]; then
        _format_do_test
    fi
}

_do_lsof() {
    local OPTIND arg
    local do_format=0
    local do_typecheck=0
    local do_stdio=0
    local total=0
    while getopts ":c:tf" arg; do
        case "${arg}" in 
            t) do_typecheck=1;;
            f) do_format=1;;
            c) do_stdio=$OPTARG;;
        esac
    done

    if [ $do_stdio == 0 ] && [ $do_typecheck == 0 ] && [ $do_format == 0 ]; then
        for key in ${!stdio_grade_map[@]}; do
            transmit $STDIO_TEST $key ${stdio_grade_map[$key]}
        done
        transmit $TYPECHECK_TEST $type_message $type_score
        transmit $FORMAT_TEST $format_message $format_score
    elif [[ -v "stdio_grade_map[$do_stdio]" ]]; then
        transmit $STDIO_TEST $do_stdio ${stdio_grade_map[$do_stdio]}
    elif [ "$do_typecheck" == 1 ]; then
        transmit $TYPECHECK_TEST $type_message $type_score
    elif [ "$do_format" == 1 ]; then
        transmit $FORMAT_TEST $format_message $format_score
    fi
    
    _sum_score # store in $grade
    ((total = $grade/$num_tests))
    transmit $ALL $main_file $total
}

_parse_arg() {
    if [ "$1" == "test" ]; then
        shift 1
        _do_test "$@"
    elif [ "$1" == "lsof" ]; then
        shift 1
        _do_lsof "$@"
    fi
    end_transmission
}

_sum_score() {
    grade=0
    for score in ${stdio_grade_map[@]}; do
        ((grade = grade + $score))
    done
    ((grade = grade + $type_score))
    ((grade = grade + $format_score))
}

service_main() {
    rm -rf stdio_tests/outputs/*
    while true; do
        read request;
        _parse_arg $request
    done
}

######## Init -> ########


OPTS=$(getopt -o :hs --long share,help,serve,clean -n 'parse-options' -- "$@")

if [ "$?" != 0 ]; then
    echo "Failed parsing options." >&2
    exit 1
fi

SERVICE_MODE=0

while true; do
    case "$1" in
        -s ) 
            SERVICE_MODE=1
            shift;;
        -h | --help )
            print_help
            exit 0
            shift;;
        --share )
            source .admin_files/tor-init.sh
            tor_init
            exit 0
            ;;
        --clean )
            source .admin_files/tor-init.sh
            tor_stop
            exit 0
            ;;
        --serve )
            wsport=$(free_port)
            timeout 3h python3 .admin_files/service-runner.py $wsport
            exit 0
            ;;
        * )
            break
            ;;
    esac
done




if [ $SERVICE_MODE == 1 ]; then
    # type enums
    ALL=0
    STDIO_TEST=1
    TYPECHECK_TEST=2
    FORMAT_TEST=3
    ARG_TEST=4 # Not yet implemented
    UNIT_TEST=5 # Not yet implemented

    # Stores *_test.txt:score
    declare -A stdio_grade_map
    format_score=0
    format_message="n/a" # padding
    type_score=0
    type_message="n/a" # padding
    grade=0
    num_tests=2
    for testpath in stdio_tests/inputs/*.txt; do
        filename=$(basename $testpath)
        stdio_grade_map[$filename]=0
        ((num_tests = num_tests+1))
    done
    service_main
    exit 0
fi

# Clears the screen
tput reset
BLACK=$(tput setaf 0)
RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
BLUE=$(tput setaf 4)
MAGENTA=$(tput setaf 5)
CYAN=$(tput setaf 6)
WHITE=$(tput setaf 7)
ORANGE=$(tput setaf 202)
RESET=$(tput sgr0)

num_tests=0
grade=0
check_hashes

check_dependencies
welcome_message

if [ ! "$(uname)" = Linux ]; then
    echo "Run this on a Linux platform!"
    exit 1
fi

if ! [ "$annoying_nodebug" = "g" ] && ! grep 'docker\|lxc' /proc/1/cgroup >/dev/null 2>&1; then
    if [ "$language" = "cpp" ]; then
        :
        # TODO Find a good C++ flowchart generator and run here
    elif [ "$language" = "python" ]; then
        section
        echo You may find the following Control Flow Graphs helpful in thinking about your code:
        for pyfile in *.py; do
            py2cfg "$pyfile"
        done
        ls ./*.svg
        echo -e "\nDo you want to show the control flow graphs for your code?"
        echo "Type y or n, then$CYAN Enter.$RESET"
        read -r cfg_open
        [ -z "$cfg_open" ] && cfg_open="n"
        if [ "$cfg_open" = "y" ]; then
            for svgfile in ./*.svg; do
                xdg-open "$svgfile"
            done
        fi
    elif [ "$language" = "bash" ]; then
        :
        # TODO A flowchart generator for bash does not likely exist...
    elif [ "$language" = "rust" ]; then
        :
        # TODO a flowchart generator?
    fi
fi
######## <- Init ########

######## Standard tests -> ########
shopt -s nullglob
cfg_tests
unit_tests
doctest_tests "$main_file"
stdio_tests "$main_file"
arg_tests "$main_file"
files_exist file_arr
shopt -u nullglob
######## <- Standard tests ########

######## Static analysis -> ########
if [ "$enable_static_analysis" = true ]; then
    section
    echo -e "$ORANGE""Below here, we present suggestions from automatic static analysis:""$RESET"
    keepgoing
    subsection
    if [ "$language" = "cpp" ]; then
        echo "Running command: $" "$MAGENTA cppcheck --enable=all --error-exitcode=1 --language=c++ ./*.cpp ./*.h ./*.hpp$RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA man cppcheck$RESET and $ $MAGENTA cppcheck --help$RESET for more detail.\n"
        cppcheck --enable=all --error-exitcode=1 --language=c++ ./*.cpp ./*.h ./*.hpp
    elif [ "$language" = "python" ]; then
        echo "Running command: $" "$MAGENTA mypy --strict --disallow-any-explicit ./*.py$RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA man mypy$RESET and $ $MAGENTA mypy --help$RESET for more detail.\n"
        mypy --strict --disallow-any-explicit ./*.py
    elif [ "$language" = "bash" ]; then
        echo "Running command: $" "$MAGENTA shellcheck --check-sourced --external-sources $main_file $RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA man shellcheck$RESET and $ $MAGENTA shellcheck --help$RESET for more detail.\n"
        shellcheck --check-sourced --external-sources "$main_file"
    elif [ "$language" = "rust" ]; then
        :
        # cargo check
        # This is just basically the first step of compilation by cargo run...
        # Is there any more deep check? I doubt it...
    fi
    grade_update "static analysis / typechecking" 100 0
fi
######## <- Static analysis ########

######## Format check -> ########
if [ "$enable_format_check" = true ]; then
    section
    echo -e "$ORANGE""Below here, we present suggestions about automatic code formatting style:""$RESET"
    keepgoing
    subsection
    shopt -s nullglob
    if [ "$language" = "cpp" ]; then
        echo "Running command: $" "$MAGENTA python3 .admin_files/run-clang-format.py -r --style=LLVM --exclude './.admin_files/build/*' .$RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA"" clang-format --help$RESET for more detail.\n"
        python3 .admin_files/run-clang-format.py -r --style=LLVM --exclude './.admin_files/build/*' .
        # CI needed this script instead of local command?
        # clang-format --dry-run --Werror --style=Microsoft *.cpp *.h *.hpp
    elif [ "$language" = "python" ]; then
        echo "Running command: $" "$MAGENTA black --check ./*.py$RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA man black$RESET and $ $MAGENTA black --help$RESET for more detail."
        [ $annoying_nodebug = 'd' ] && echo -e "If you are passing locally, but not on the server, or vice-versa, check: $MAGENTA black --version$RESET\n"
        black --check ./*.py
    elif [ "$language" = "bash" ]; then
        echo "Running command: $" "$MAGENTA shfmt -i 4 -d .$RESET"
        # https://www.arachnoid.com/linux/beautify_bash/
        # https://github.com/mvdan/sh#shfmt
        # Format dir with: ./go/bin/shfmt -i 4 -w .
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA"" shfmt --help$RESET for more detail.\n"
        if [ -x ./shfmt ]; then
            ./shfmt -i 4 -d .
        elif command -v shfmt >/dev/null 2>&1; then
            shfmt -i 4 -d .
        else
            echo "Install shfmt to run the Bash format check!"
            (exit 1)
        fi
    elif [ "$language" = "rust" ]; then
        echo "Running command: $" "$MAGENTA rustfmt --check ./src/*.rs$RESET"
        [ $annoying_nodebug = 'd' ] && echo -e "\nSee $ ""$MAGENTA rustfmt --help$RESET for more detail."
        if command -v rustfmt >/dev/null 2>&1; then
            rustfmt --check ./src/*.rs
        elif [ -x ./.admin_files/rustfmt ]; then
            ./.admin_files/rustfmt --check ./src/*.rs
        else
            echo "Install rustfmt to run the rust format check!"
            (exit 1)
        fi
    fi
    grade_update "auto-format style check" 100 0
    shopt -u nullglob
fi
######## <- Format check ########

######## Variable custom tests -> ########
first="0"
for func in $(compgen -A function); do
    if grep -q "^custom_test" - <<<"$func"; then
        if [ $first = "0" ]; then
            section
            echo "$ORANGE""Below here, we present custom tests.""$RESET"
            echo "If you want to see what's happening in the custom tests,"
            echo "then read the tests themselves at the bottom of the grade.sh file."
            first="1"
            keepgoing
        fi
        custom_test_score=-1
        check_hashes
        subsection
        echo "* Doing custom test: $func"
        $func
        check_hashes
        if [ "$custom_test_score" -ne "-1" ]; then
            grade_update "$func" "$custom_test_score" 0
        fi
    fi
done
######## <- Variable custom tests ########

######## Cleanup -> ########
rm -rf __pycache__
rm -rf .admin_files/__pycache__
rm -rf .admin_files/build
rm -rf .mypy_cache
rm -rf unit_tests/.mypy_cache
######## <- Cleanup ########

######## Reporting and grading -> ########
grade=$(echo "print(int($grade / $num_tests))" | python3)
echo -e "Your total grade is:\n$grade" >$student_file
section
cat $student_file
if [ -f student_git_pass_threshold.txt ]; then
    pass_threshold=$(cat student_git_pass_threshold.txt)
    check_hashes
else
    pass_threshold=70
fi
notdone=$(python3 -c "print($grade < $pass_threshold)")
check_hashes
perfect=$(python3 -c "print($grade == 100)")
echo -e "\n"$CYAN"The last step is to push your changes to git-classes:"
echo -e "    Make sure the CI passes, turns green, and you inspect the job itself for your grade."
echo -e "    Your repo will turn green with a grade of $pass_threshold (set in student_git_pass_threshold.txt)."
echo -e "    Actually look at the Gitlab CI details to see what we think your current grade is."
echo -e "    Unless you see the output and numerical grade above on Gitlab, do not assume you are done!\n""$RESET"
if [ "$notdone" == "True" ]; then
    echo -e "$RED\nYou're not passing yet.$RESET"
    echo -e "To see why, run this script in debug mode!\n"
    exit 1
else
    if [ "$perfect" == "True" ]; then
        echo -e "$GREEN\nIt's perfect. Congratulations!\n$RESET"
    else
        echo -e "$GREEN\nYou're passing; decide how much you want the extra points!\n$RESET"
    fi
    exit 0
fi
######## <- Reporting and grading ########
