#! /bin/bash

source .admin_files/util.sh

if [ ! "$TIMEOUT" ]; then
    TIMEOUT="3h"
fi

tor_init() {
    if [ -f ".torrc" ]; then
        echo "Service is running";
        exit 1
    fi
    local socks_port http_port ws_port tor_fd line
    socks_port=$(free_port)
    http_port=$(free_port $((socks_port+1)))
    if [ -f ".lock" ]; then
        # If the service is already running, get the port number from lock file
        ws_port=$(cat .lock \
            | grep "window.service_port=*" \
            | awk 'BEGIN { FS="="}; {print $2}')
    else
        ws_port=$(free_port $((http_port+2)))
    fi

    trap tor_stop SIGINT

    # Create the .torrc file for this shareable instance.
    echo "SocksPort 127.0.0.1:$socks_port" > ".torrc"
    echo "HiddenServiceDir .hidden" >> ".torrc"
    echo "HiddenServicePort 80 127.0.0.1:$http_port" >> ".torrc"
    echo "HiddenServiceDir .hidden/ws" >> ".torrc"
    echo "HiddenServicePort 80 127.0.0.1:$ws_port" >> ".torrc"
    coproc tor_fd { timeout $TIMEOUT ./.admin_files/Tor/tor -f .torrc; }

    while true; do
        read <& "${tor_fd[0]}" line;
        if [ "$?" != 0 ]; then
            break;
        elif [[ "$line" == *"(done)"* ]]; then
            # Serve index.html
            timeout $TIMEOUT python3 -m http.server $http_port 2>/dev/null &

            if [ ! -f ".lock" ]; then 
                # Start websocket service if it isn't running already
                timeout $TIMEOUT python3 .admin_files/service-runner.py $ws_port & 
                # Wait for the proc to init lock
                while [ ! -f ".lock" ]; do
                    sleep 1
                done
            fi;

            # Add onion link to lock file so it can be sourced by browser
            echo "" >> ".lock"
            echo "window.onion_link='$(cat .hidden/ws/hostname)'" >> ".lock"

            # Share
            clear
            echo "--- grade-sh/tor ---"
            echo ""
            echo "Share link: $(cat .hidden/hostname)"
            echo "Local link: localhost:$http_port"
            echo ""
            echo "To copy:"
            echo "    ctrl + shift + c"
            echo ""
            echo "To exit:"
            echo "    ctrl + c"
            echo "--------------------"

        fi;
        echo $line
    done
    # timeout expired
    tor_stop
}

_tor_stop() {
    sleep 1
    
    # To produce null strings when no match
    shopt -s nullglob   
    if [ ! -f ".torrc" ]; then
        return
    fi    
    local line port pid
    port=$(cat .torrc | awk 'NR==1{print $2}' | awk 'BEGIN {FS=":"}; {print $2}');
    pid=$(lsof -i:$port | awk 'NR>1{print $2}')
    kill -15 $pid 2>/dev/null

    # kill HTTP/WS
    for line in $(cat .torrc | grep "HiddenServicePort" | awk '{print $3}'); do
        port=$(echo $line | awk 'BEGIN {FS=":"}; {print $2}');
        for pid in $(lsof -i:$port | awk 'NR>1{print $2}'); do
            kill -15 $pid 2>/dev/null
        done
    done
    
    rm -rf .hidden 2>/dev/null
    rm .torrc 2>/dev/null
    rm .lock  2>/dev/null
}

tor_stop() {
    trap '' SIGINT
    _tor_stop &
    exit 
}