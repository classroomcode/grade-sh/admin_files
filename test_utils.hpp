#ifndef TEST_UTILS_H
#define TEST_UTILS_H

#ifdef __cplusplus
#include <exception>
#include <iostream>
#include <stdexcept>
#include <string>

template <typename Test>
int test_wrapper(const int argc, const char **argv, Test &&test) {
  if (argc < 2) {
    throw std::invalid_argument(
        "Must pass desired returncode as commandline argument");
  }
  const int desired_retcode = std::stoi(argv[1]);
  bool result = false;
  try {
    result = test();
  } catch (std::exception &e) {
    std::cerr << e.what() << std::endl;
  } catch (...) {
    std::cerr << "An exception of unknown type was thrown" << std::endl;
  }
  return result ? desired_retcode : 1;
}
#else

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

typedef bool (*TestHandler)();

static inline int test_wrapper(int argc, const char *argv[], TestHandler test) {
  if (argc < 2) {
    fprintf(stderr, "Must pass desired returncode as commandline argument");
    return -1;
  }
  int retcode = atoi(argv[1]);
  if (retcode == 0) {
    fprintf(stderr, "Invalid return code");
    return -1;
  }
  if (test()) {
    exit(retcode);
  }
  exit(1);
}

#endif
#endif
