build() {
    log ".admin_files/builder/$language/init.sh does not implement 'build' method"
    exit 1
}

build_generic() {

    local dirname=$(basename $PWD)
    local lockfile="/tmp/build-$dirname.lock"
    if [ -f ".admin_files/builder/$language/init.sh" ]; then
        source ".admin_files/builder/$language/init.sh"
        # Acquire mutex during build
        (
            flock -xn 3
            if [ "$?" != 0 ]; then
                exit 0
            fi
            trap "rm $lockfile" EXIT
            log "Building $main_file..."
            build
            exit "$?"
        ) 3>$lockfile
        if [ "$?" != 0 ]; then
            log "Build failed"
            exit 1
        fi
    fi
}
