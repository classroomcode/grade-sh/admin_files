#! Build C/C++

build() {
    if [ -f "build_env.sh" ]; then
        source build_env.sh
    fi
    
    export CC_FLAGS
    export CPP_FLAGS
    export LD_FLAGS
    export main_file
    make -f ".admin_files/builder/cpp/makefile" -j >&2
    return $?    
}
