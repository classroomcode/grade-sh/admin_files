
import sqlite3
import shlex
import docopt
import sys
import time
import signal
import textwrap
import os

END = "\033[0m"
YELLOW = "\033[33m"
MAGENTA = "\033[35m"
GREEN = "\033[32m"
RED = "\033[31m"
CYAN="\033[96m"

end_transmission = lambda: print("<end transmission>")

command_usage = {
    "register": "Usage: register <testclass> <testname> <command>",
    "command": "Usage: command <testclass> <testname>",
    "update": "Usage: update <testclass> <testname> <score> <runtime>",
    "lsof": "Usage: lsof (--testclass <testclass>| <testclass> <testname>) <attr>...",
    "weight": "Usage: weight <testclass> <weight>",
    "render": (textwrap.dedent(
        """
        Usage: render <testclass> <testname> [--all | --header | --result | --summary] [--width WIDTH]

        Options:
            --width <WIDTH>  Truncate line to width [default: 120]
        """.strip("\n"))
    ),
    "capture": "Usage: capture <testclass> <testname> [--echo]",
    "quit": "Usage: quit",
    "report": "Usage: report [--file=<FILE>]",
    "testattr": "Usage: testattr <testclass> [--weight=<N>] [--priority=<N>]",
    "testclass": "Usage: testclass",
    "testnames": "Usage: testnames <testclass>",
    "grade": "Usage: grade [--summary]"
}

with sqlite3.connect(":memory:") as conn:
    cursor = conn.cursor()
    cursor.execute(r"""
        CREATE TABLE Manifest (
            type,
            name,
            command,
            PRIMARY KEY (type, name)
        )
    """)
    cursor.execute(r"""
        CREATE TABLE Results (
            type,
            name,
            score int,
            runtime,
            output BINARY,
            PRIMARY KEY (type, name)
        )
    """)
    cursor.execute(r"""
        CREATE TABLE TestAttr (
            type,
            priority DEFAULT 1,
            weight DEFAULT 1,
            PRIMARY KEY (type)
        )
    """)

    def register(args):
        """Register <testclass> <testname> as a known test"""

        testclass, testname, command = args["<testclass>"], args["<testname>"], args["<command>"]
        cursor.execute(r"""
            INSERT INTO Manifest (
                type, name, command
            ) VALUES (
                ?, ?, ?
            )
        """, (testclass, testname, command))
        cursor.execute(r"""
            INSERT INTO Results (
                type, name, score, runtime, output
            ) VALUES (
                ?, ?, ?, ?, ?
            )
        """, (testclass, testname, 0, 0.0, ""))
        try:
            cursor.execute("""
                INSERT INTO TestAttr (
                    type
                ) VALUES (
                    ?
                ) 
            """, (testclass, ))
        except sqlite3.IntegrityError:
            pass
    
    def command(args):
        """Return the command used to execute the test"""

        testclass, testname = args["<testclass>"], args["<testname>"]
        print(cursor.execute(r"""
            SELECT command FROM Manifest
            WHERE type=? AND name=?
        """, (testclass, testname)).fetchall()[0][0])
    
    def update(args):
        """Set result of <testclass> <testname>"""

        testclass, testname, score, runtime = (
            args["<testclass>"],
            args["<testname>"],
            args["<score>"],
            args["<runtime>"],
        )
        cursor.execute(r"""
            UPDATE Results 
            SET score = ?, runtime = ?
            WHERE type = ? AND name = ? 
        """, (score, runtime, testclass, testname))

    def weight(args):
        testclass, weight = args["<testclasss>"], args["<weight>"]
        cursor.execute(r"""
            INSERT INTO TestAttr (
                type, weight
            )
        """, (testclass, weight))

    def lsof(args):
        """Retrieves attributes of <testclass> <testname>"""
        
        testclass, name, attr = (
            args["<testclass>"],
            args["<testname>"],
            args["<attr>"]
        )
        
        def to_string(val):
            if isinstance(val, bytes):
                return str(val, encoding="utf-8")
            else:
                return str(val)

        if name is not None:
            print(" ".join(
                to_string(val) for val in cursor.execute(f"""
                    SELECT {",".join(attr)} FROM Results
                    WHERE type=? AND name=?
                """, (testclass, name)).fetchall()[0]))
        else:
            for line in cursor.execute(f"""
                    SELECT {','.join(attr)} FROM Results
                    Where type=?
                """, (testclass, )
            ).fetchall():
                print(" ".join(to_string(val) for val in line))
        end_transmission()


    def render(args):
        """Generate test report"""

        testclass, testname = args["<testclass>"], args["<testname>"]
        command = cursor.execute("""
            SELECT command FROM Manifest
            WHERE type = ? AND name = ?
        """, (testclass, testname)).fetchall()[0][0]
        
        score, runtime, output = cursor.execute("""
            SELECT score, runtime, output from Results
            WHERE type = ? AND name = ?
        """, (testclass, testname)).fetchall()[0]

        if isinstance(output, bytes):
            output = str(output, encoding="utf-8")
        
        try:
            width = int(args["--width"])
        except:
            print("Invalid argument: failed to cast '--width' argument to int", file=sys.stderr)
            return
             
        def print_header():
            print("=" * width)
            inner = f"{testclass} - {testname}"[:width]
            print(f"{YELLOW}{inner}{END}")
            print("-" * width)
            inner = f"$ {command}"[:width]
            print(f"{MAGENTA}{inner}{END}")
            print("-" * width)

        def print_result():
            if 0 <= score < 70:
                color = RED
            elif 70 <= score < 90:
                color = YELLOW
            elif 90 <= score <= 100:
                color = CYAN
            
            print() # Add space after program output
            print(f"Completed in: {runtime}")
            print(f"Score:        {color}{score}{END}")

        if args["--all"]:
            print_header()
            output_lines = [line[:width] for line in output.split("\n")]
            print("\n".join(output_lines))
            print_result()
        elif args["--header"]:
            print_header()
        elif args["--result"]:
            print_result()
        
        end_transmission()
    
    def testattr(args):
        """Set <testclass> attributes"""

        testclass = args["<testclass>"]
        priority, weight = 1, 1
        if args["--priority"] is not None:
            try:
                priority = int(args["--priority"])
            except:
                print("Failed to cast --priority argument to int", file=sys.stderr)
                return
        if args["--weight"] is not None:
            try:
                weight = int(args["--weight"])
            except:
                print("Failed to cast --weight argument to int", file=sys.stderr)
        
        cursor.execute("""
            UPDATE TestAttr
            SET PRIORITY = ?, WEIGHT = ?
            WHERE type = ?
        """, (priority, weight, testclass))


    def testclass(args):
        """Return a list of registered test classes, sorted by priority"""
        
        result = cursor.execute("""
            SELECT type, priority FROM TestAttr
        """).fetchall()
        print(" ".join(item[0] for item in sorted(result, key=lambda item: item[1])))
    
    def testnames(args):
        result = cursor.execute("""
            SELECT name FROM Manifest
            WHERE type = ?
        """, (args["<testclass>"], )).fetchall()
        print(" ".join(i[0] for i in result))
    
    def grade(args):
        """
        Returns the grade and attributes of all tests. Can be piped to
        column command
        """
        
        total_score = 0
        weight_sum = 0

        if args["--summary"]:

            print("TYPE NAME RUNTIME WEIGHT SCORE")
        for testclass, weight, _ in sorted(
            cursor.execute("""
                SELECT type, weight, priority FROM TestAttr
            """
            ).fetchall(), key=lambda x: x[-1]
        ):
            weight_sum += weight
            
            for name, score, runtime in cursor.execute("""
                SELECT name, score, runtime FROM Results
                WHERE type = ?
                """, (testclass, )
            ).fetchall():
                total_score += score * weight
                weight_sum += 100 * weight
                
                if args["--summary"]:
                    print(testclass, name, "{:0.4f}".format(float(runtime)), weight, score)
            
            if args["--summary"]:
                print("-")
        
        total = round((total_score/weight_sum) * 100)
        if 0 <= total < 70:
            score_str = f"{RED}{total}{END}"
        elif 70 <= total < 90:
            score_str = f"{YELLOW}{total}{END}"
        elif 90 <= total <= 100:
            score_str = f"{CYAN}{total}{END}"
        
        if args["--summary"]:
            print("- - - - Grade")
            print("- - - -", score_str)
            end_transmission()
        else:
            print(total)

    capture_target = []
    capture_result = []
    echo_capture = False
    def Handle_SIGUSR1(*args):
        global echo_capture

        if capture_target:
            captured_bytes = bytes(
                "\n".join(capture_result),
                encoding="utf-8")
            cursor.execute(
                """
                UPDATE Results
                SET output = ?
                WHERE type = ? AND name = ?
                """,
                (captured_bytes, *capture_target[0])
            )
            capture_target.clear(), capture_result.clear()
            echo_capture = False
        else:
            print(f"Received signal: {signal.SIGUSR1}", file=sys.stderr)
    
    def capture(args):
        global echo_capture

        testclass, testname = args["<testclass>"], args["<testname>"]
        if capture_target:
            print("Already capturing...", file=sys.stderr)
            return
        else:
            echo_capture = args["--echo"]
            capture_target.append((testclass, testname))
            print(os.getpid(), int(signal.SIGUSR1))
        
    
    def Handle_SIGTERM(*args):
        exit(0)
    
    signal.signal(signal.SIGUSR1, Handle_SIGUSR1)
    
    quit = lambda *args: exit(0)

    while True:
        if capture_target:
            try:
                input_val = input()
            except EOFError:
                time.sleep(0.1)
                continue
            else:
                if echo_capture:
                    print(input_val, file=sys.stderr)

                capture_result.append(input_val)
        else:
            try:
                input_param = shlex.split(input())
            except EOFError:
                exit(0)
            except KeyboardInterrupt:
                pass
            
            try:
                command_req = input_param[0]
            except IndexError:
                continue
                
            if command_req not in command_usage:
                print(f"Unknown command '{command_req}'", file=sys.stderr)
                continue
            
            try:
                opts = docopt.docopt(
                    command_usage[command_req],
                    argv=input_param[1:],
                    help=False # Don't exit on help
                )
            except docopt.DocoptExit:
                print(command_usage[command_req], file=sys.stderr)
                continue
            else:
                globals()[command_req](opts)

