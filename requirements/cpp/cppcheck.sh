# Can't use package manager on remote VM's so build from source

# Fails on WSL2 Ubuntu 20.04, but works on Non-WSL Ubuntu 20.04

echo "Installing cppcheck..."

add_path "$HOME/bin"
(
    git clone https://github.com/danmar/cppcheck ~/cppcheck
    cd ~/cppcheck
    make MATCHCOMPILER=yes FILESDIR=$HOME/.config/cppcheck HAVE_RULES=yes CXXFLAGS="-O2 -DNDEBUG -Wall -Wno-sign-compare -Wno-unused-function"
    if [ "$?" != 0 ]; then
        exit 1
    fi
    if [ ! -d "$HOME/bin" ]; then
        mkdir "$HOME/bin"
    fi
    cp cppcheck "$HOME/bin/cppcheck"
)
exit "$?"
