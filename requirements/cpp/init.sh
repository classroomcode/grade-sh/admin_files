# Install C/C++ dependencies

install() {
    local failed=0
    for package in $(cat .admin_files/requirements/cpp/requirements.txt); do
        if ! command -v $package &>/dev/null; then
            if [ -f ".admin_files/requirements/cpp/$package.sh" ]; then
                # Install from source
                source ".admin_files/requirements/cpp/$package.sh";
            else
                echo Install $package before proceeding.
                return 1
            fi
        fi
    done;
    if [ "$failed" = 1 ]; then
        echo "Use your Linux system's package manager (apt/zypper/dnf/etc)."
        return 1
    fi
}
