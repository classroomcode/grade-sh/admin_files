# Install language specific dependencies

install() {
    echo "ERROR .admin_files/requirements/$language/init.sh  does not define 'install' method"
    exit 1
}

install_generic() {

    local dirname=$(basename $PWD)
    local lockfile="/tmp/install-$dirname.lock"

    (
        flock -xn 3
        if [ "$?" != 0 ]; then
            exit 1
        fi
        trap "rm $lockfile" EXIT
        if [ -f ".admin_files/requirements/$language/init.sh" ]; then
            source ".admin_files/requirements/$language/init.sh"
            
            echo "Installing $language dependencies..."

            if (install); then 
                echo "Installed $language dependencies :)"
            else
                echo "Failed to install $language dependencies :("
                exit 2
            fi
        fi
    ) 3> "$lockfile"
}

# install docopts for shell
add_path "~/bin"
cp .admin_files/docopts ~/bin