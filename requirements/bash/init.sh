# Install bash dependencies

install() {
    local failed=0
    for package in $(cat .admin_files/requirements/bash/requirements.txt); do
        if ! command -v $package &>/dev/null; then
            if [ -f ".admin_files/requirements/bash/$package.sh" ]; then
                if ! source ".admin_files/requirements/bash/$package.sh"; then
                    echo "Failed to build $package"
                fi   
            else
                echo Install $package before proceeding.
                failed=1
            fi
        fi
    done
    
    if [ "$failed"=1 ]; then
        echo "Use your Linux system's package manager (apt/zypper/dnf/etc)."
        exit 1
    fi
}
