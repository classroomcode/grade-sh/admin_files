#
# Install shfmt

if [ ! -d "$HOME/bin" ]; then
    mkdir "$HOME/bin"
fi

add_path "$HOME/bin"
wget -O "$HOME/bin" https://github.com/mvdan/sh/releases/download/v3.1.2/shfmt_v3.1.2_linux_amd64
chmod +x "$HOME/bin/shfmt"
