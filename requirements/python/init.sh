# Install python dependencies

install() {
    # Add ~/.local/bin to path
    add_path "$HOME/.local/bin"

    # Install grade.sh specific dependencies
    pip3 install -r .admin_files/requirements/python/requirements.txt --user --upgrade
    if [ "$?" != 0 ]; then
        exit 1
    fi
    # Install assignment specific dependencies
    # Perhaps it's more sensible to use a virtual env?
    if [ -f "requirements.txt" ]; then
        pip3 install -r requirements.txt  --user --upgrade
        if [ "$?" != 0 ]; then
            echo "Failed to install python packages"
        fi
        exit "$?"
    fi
    exit "$?"
}
