
format_test.register()
{
    database.register "format_test" "format_test" "$format_command"
}

format_test.runTest()
{
    check_hashes
    database.get_test_command testcmd "format_test" "format_test"
    database.capture_start "format_test" "format_test" $2
    timer_start
    eval "${testcmd[@]}" >& ${TESTDATA[1]} 2>& ${TESTDATA[1]}
    code=$?
    timer_stop
    database.capture_stop
    
    if [ "$code" = 0 ]; then
        database.update "format_test" "format_test" 100 $D_TIME
    else
        database.update "format_test" "format_test" 0 $D_TIME
    fi
    return $code
}
