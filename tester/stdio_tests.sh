stdio_tests.register()
{
    for input_file in stdio_tests/inputs/*_input.txt; do
        filename=$(basename $input_file)
        testname=${filename%_*}
        
        # Compiled binaries
        if [ -z "$interpreter" ]; then
            run_prog="./$main_file"
        else
            run_prog="$interpreter $main_file"
        fi

        cmd_str="timeout $process_timeout $run_prog $main_file_arguments < $input_file > stdio_tests/outputs/${testname}_output.txt"
        database.register stdio_tests $testname "$cmd_str"
    done
}

stdio_tests.runTest()
{
    
    local testname=$1
    local output goal
    
    check_hashes
    build
    database.get_test_command testcmd stdio_tests $testname
    database.capture_start stdio_tests $testname $2
    timer_start
    eval "${testcmd[@]}" 2>& ${TESTDATA[1]} # run main and capture stderr
    code="$?"
    timer_stop
    database.capture_stop
    
    output="stdio_tests/outputs/${testname}_output.txt"
    goal="stdio_tests/goals/${testname}_output.txt"

    # Score is based on this
    diff -y -W 200 $goal $output >stdio_tests/diffs/$testname.txt
    diff_result="$?"
    
    # Generate acceptable diff. This may take a while so detach the process
    nohup  vim -d $goal $output \
        -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
        -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White' \
        -c TOhtml \
        -c "w! stdio_tests/diffs/$testname.html" \
        -c 'qa!' >/dev/null 2>&1 &


    # Main failed so just return
    if [ "$code" != "0" ]; then
        database.update stdio_tests $testname 0 $D_TIME
        return $code
    fi

    if [ "$fuzzy_partial_credit" = false ]; then
        if [ "$diff_result" = 0 ] ; then
            database.update stdio_tests $testname 100 $D_TIME
            return 0
        else
            database.update stdio_tests $testname 0 $D_TIME
            return 1
        fi
    else
        database.update stdio_tests $testname $(python3 .admin_files/fuzzydiffer.py $goal $output) $D_TIME
        # How to handle fuzzy diff?
        return 0
    fi
}


stdio_tests.doAfter()
{
    if $DEBUG; then
        local test_id="$@"
        testname=${test_id%_*}
        read -p "Press ${CYAN}Enter${RESET} to see diff: "
        show_diff "stdio_tests/goals/${testname}_output.txt" "stdio_tests/outputs/${testname}_output.txt"
        debug_cmd "$main_file" "$main_file_arguments" "stdio_tests/inputs/${testname}_input.txt"
        return 0
    fi   
}

