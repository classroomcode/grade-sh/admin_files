
static_test.register()
{
    database.register "static_test" "static_test" "$static_analysis_command"
}

static_test.runTest()
{   
    check_hashes
    database.get_test_command testcmd "static_test" "static_test"
    database.capture_start "static_test" "static_test" $2
    timer_start
    eval "${testcmd[@]}" >& ${TESTDATA[1]} 2>& ${TESTDATA[1]}
    code="$?"
    timer_stop
    database.capture_stop

    if [ "$code" = 0 ]; then
        database.update "static_test" "static_test" 100 $D_TIME
    else
        database.update "static_test" "static_test" 0 $D_TIME
    fi
}
