unit_tests.register()
{
    for testfile in unit_tests/*; do
        testname=$(basename $testfile)
        testname=${testname%.*}
        if [ -z "$interpreter" ]; then
            testfile="./unit_tests/${testname}"
        fi
        database.register unit_tests $testname "timeout $process_timeout $interpreter $testfile"
    done
}

unit_tests.python.register()
{
    for testfile in unit_tests/*; do
        testname=$(basename $testfile)
        testname=${testname%.*}
        if [ -z "$interpreter" ]; then
            testfile="./unit_tests/${testname}"
        fi
        database.register unit_tests $testname "timeout $process_timeout $interpreter .admin_files/test_utils.py $testfile"
    done
}

unit_tests.cpp.build()
{
    local filename="$1"
    local cc flags extension no_ext

    extension="${filename##*.}"
    no_ext="${filename%.*}"

    # Build unit test main

    if [ "$extension" = "c" ]; then
        cc="gcc"
        flag="$CC_FLAGS" # loaded by build_env.sh
    elif [ "$extension" = "cpp" ]; then
        cc="g++"
        flag="$CPP_FLAGS"
    else
        log "Invalid file extension"
        exit 1
    fi
    # Compile to object
    if ! $cc -c unit_tests/$filename -o unit_tests/${no_ext}.o -I. $flag; then
        log "Failed to compile $filename"
    fi
    
    # Get all objects (which should be built by build command already) that doesn't
    # define main...
    NULL_GLOB
    local no_main=()
    for object in *.o; do
        if ! objdump -d "$object" | grep "main" > /dev/null; then
            no_main+=($object)
        fi
    done
    # ...and link with our unit test main
    if ! g++ "unit_tests/${no_ext}.o" "${no_main[@]}" -o "unit_tests/${no_ext}" $LD_FLAGS> /dev/null; then
        log "Linking ${no_ext}.$extension failed"
        exit 1
    fi

    rm unit_tests/${no_ext}.o 2>/dev/null
}

unit_tests.cpp.clean()
{
    local testname=$1;
    rm unit_tests/$testname 2>/dev/null;
}

alias unit_tests.c.build="unit_tests.cpp.build"
alias unit_tests.c.clean="unit_tests.cpp.clean"

unit_tests.runTest()
{
    local testname=$1; shift;

    check_hashes
    if command -v unit_tests.$language.build >/dev/null; then
        local file;
        file="unit_tests/$testname.*"
        unit_tests.$language.build $(basename $file)
    fi

    retval=$(((RANDOM % 250) + 5))
    database.get_test_command testcmd unit_tests $testname
    testcmd+=($retval)
    # Not redirecting stdin/stdout so we want to capture outputs
    database.capture_start unit_tests $testname $@
    timer_start
    eval "${testcmd[@]}" >& ${TESTDATA[1]} 2>& ${TESTDATA[1]}
    code="$?"
    timer_stop
    database.capture_stop

    if command -v unit_tests.$language.clean >/dev/null; then
        unit_tests.$language.clean $testname
    fi
    
    if [ "$code" = "$retval" ]; then
        database.update unit_tests $testname 100 $D_TIME
        return 0
    else
        database.update unit_tests $testname 0 $D_TIME
        return 1
    fi
}


unit_tests.doAfter()
{
    if $DEBUG; then
        read -p "Press ${CYAN}Enter${RESET} to start debugger: "
        filename="unit_tests/$1*"
        debug_cmd $filename 10
    fi
}
