

arg_tests.register()
{
    for arg_file in arg_tests/args/*_args.txt; do
        filename=$(basename $arg_file)
        testname=${filename%_*}
        
        if [ -f "arg_tests/inputs/${testname}_input.txt" ]; then
            # Input file was specified which means the arg_test specifies just 
            # the output file
            cmd_str="timeout $process_timeout $interpreter $main_file $main_file_arguments \
                $(cat $arg_file) < arg_tests/inputs/${testname}_input.txt"
        else
            # Otherwise no redirections
            cmd_str="timeout $process_timeout $interpreter $main_file $main_file_arguments \
                $(cat $arg_file)"
        fi
        #TODO specify outputs but no inputs, so need to read from file in-lieu 
        # of stdin
        database.register arg_tests $testname "$cmd_str"
    done
}

arg_tests.runTest()
{

    local testname=$1
    local output goal

    check_hashes
    build
    database.get_test_command testcmd arg_tests $testname
    database.capture_start arg_tests $testname $2
    timer_start
    eval "${testcmd[@]}" 1>&2 2>& ${TESTDATA[1]} # run main and capture stdout/stderr
    code="$?"
    timer_stop
    database.capture_stop
    
    output="arg_tests/outputs/${testname}_output.txt"
    goal="arg_tests/goals/${testname}_output.txt"

    # Score is based on this
    diff -y -W 200 $goal $output >arg_tests/diffs/$testname.txt
    diff_result="$?"
    
    # Generate acceptable diff. This may take a while so detach the process
    nohup vim -d $goal $output \
        -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
        -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
        -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White' \
        -c TOhtml \
        -c "w! arg_tests/diffs/$testname.html" \
        -c 'qa!' >/dev/null 2>&1 &


    # Main failed so just return
    if [ "$code" != "0" ]; then
        database.update arg_tests $testname 0 $D_TIME
        return $code
    fi

    if [ "$fuzzy_partial_credit" = false ]; then
        if [ "$diff_result" = 0 ] ; then
            database.update arg_tests $testname 100 $D_TIME
            return 0
        else
            database.update arg_tests $testname 0 $D_TIME
            return 1
        fi
    else
        database.update arg_tests $testname $(python3 .admin_files/fuzzydiffer.py $goal $output) $D_TIME
        # How to handle fuzzy diff?
        return 0
    fi
}