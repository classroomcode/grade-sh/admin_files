import pty
import subprocess
import struct
import fcntl
import time
import termios

# Get the master terminal size.
term_size = str(subprocess.check_output(["stty", "size"]), encoding="utf-8")
row, col = tuple(int(d) for d in term_size.split(" "))

pid, fd = pty.fork()
if pid == 0:
    # After pty fork, the child receives a new set of stdin/stdout descriptors
    # which is inherited by the subprocess below.
    subprocess.call(["tmux", "attach", "-t", "0"])
else:
    # Set slave pty to the same dimension as master. This effectively
    # 'sizes' the tmux session to that of the master.
    fcntl.ioctl(fd, termios.TIOCSWINSZ, struct.pack("HHHH", row, col, 0, 0))

    # Wait a bit for the changes to propagate before killing this process group
    time.sleep(0.5)
