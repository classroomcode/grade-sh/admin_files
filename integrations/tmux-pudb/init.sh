#! /bin/bash

tmux-pudb() {
    if tmux ls | grep "grade-sh" >/dev/null ; then
        tmux attach -t "grade-sh" 2>/dev/null
    else
        rows=$(stty size | awk '{print $1}')
        FIFO="/tmp/tmux-pudb3-grade-sh.fifo"
        rm $FIFO 2>/dev/null
        mkfifo "$FIFO";
        tmux new-session -d -s "grade-sh"
        tmux rename-window "grade-sh"
        tmux send-keys "tty > $FIFO && perl -MPOSIX -e pause" ENTER
        tmux split-window -v
        tmux resize-pane -D "$rows"
        tmux send-keys "cd $PWD && .admin_files/integrations/tmux-pudb/start-debugger.sh $FIFO $1 $2 $3" ENTER
        tmux select-pane -t 0
        tmux attach -t "grade-sh"
    fi
}
