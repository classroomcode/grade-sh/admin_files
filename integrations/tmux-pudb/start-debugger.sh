#! /bin/bash

read PANE1_TTY < "$1"
python3 .admin_files/integrations/tmux-pudb/ptyfork.py

if [ -z "$4" ]; then
    PUDB_TTY=$PANE1_TTY pudb3 "$2" "$3"
else
    PUDB_TTY=$PANE1_TTY pudb3 "$2" "$3" < "$4"
fi

tmux kill-session
