#! /bin/bash

# Default differ. Can be overloaded by subclass
show_diff() {
    read -p "To quit vimdiff: Type ${CYAN}:qa!$RESET: "
    vim -d "$1" "$2" \
    -c 'highlight DiffAdd ctermbg=Grey ctermfg=White' \
    -c 'highlight DiffDelete ctermbg=Grey ctermfg=Black' \
    -c 'highlight DiffChange ctermbg=Grey ctermfg=Black' \
    -c 'highlight DiffText ctermbg=DarkGrey ctermfg=White'
}

null_debugger () { return 1; }

# Default debugger. Can be overloaded by subclass
default_debugger() {
    if [ "$language" = "cpp" ]; then
        # http://mywiki.wooledge.org/BashFAQ/050
        debug_cmd=(null_debugger)
    elif [ "$language" = "python" ]; then
        source ".admin_files/integrations/tmux-pudb/init.sh"
        echo "$1" "$2" "$3"
        tmux-pudb "$@"
    elif [ "$language" = "bash" ]; then
        debug_cmd=(bash -x "$@")
        # TODO a real bash debugger like bashdb?
    elif [ "$language" = "bash" ]; then
        debug_cmd=(gdb '--eval-command="break main"' --args "$1")
    elif [ "$language" = "rust" ]; then
        debug_cmd=(gdb '--eval-command="break main"' --args "$1")
    fi

    ${debug_cmd[@]}
    return $?
}

start_debugger() {
    :
}

debug_cmd() {
    if $USE_DEFAULT_DEBUGGER; then
        default_debugger "$@"
    else
        start_debugger "$@"
    fi
    return $?
}

# Default warning. Can be overloaded by subclass
hash_alert() {
    section
    echo -e "$RED\nDon't edit any of the auotgrader's files, or you will fail!$RESET"
    echo "Notice the files listed below, with the long hashes in front of them."
    echo -e "Those are the files you broke.\n"
    diff .admin_files/hashes.txt .admin_files/grader_hashes.txt
    echo -e "\nIf you are seeing this, then type:"
    echo "    $ $MAGENTA git log$RESET"
    echo "Then look at the log to find the first four letters of the hash of the instructor's last commit; copy it."
    echo "Then type:"
    echo "    $ $MAGENTA git checkout firstfourlettersofthehashoftheinstructorslastcommit fileorfolderyoubroke $RESET"
    echo -e "You must do this for each file you broke (or just the super-folder of the grader-files), and then re-run grade.sh.\n"
    grade=0
    echo "You edited our stuff, and so you get 0; see the output for details" >"$student_file"
    echo $grade >>"$student_file"
}

# Configure how to activate integration
if [ "$TERM_PROGRAM" = "vscode" ]; then
    source ".admin_files/integrations/vscode/init.sh"
else
    :
fi

