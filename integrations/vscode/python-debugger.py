"""
Generate .vscode/launch.json file. Configures the vscode debugger for python
debugging
"""
import json
import copy
import os
import shlex

configurations = []

prototype = {
    "name": "",
    "type": "python",
    "request": "launch",
    "program": "${file}",
    "console": "integratedTerminal",
    "stopOnEntry": True,
    "args":None,
}

while True:
    line = list(filter(lambda x: bool(x), input().split(" ")))
    if not line:
        break
    config = copy.copy(prototype)
    if line[0] == "stdio_tests":
        test_type, input_file = line
        config["name"] = f"stdio_test: {input_file}"
        config["program"] = os.environ["main_file"]
        config["args"] = shlex.split(os.environ["main_file_arguments"]) + [
            "<", f"stdio_tests/inputs/{input_file}"
        ]
    
    elif line[0] == "unit_tests":
        test_type, filename = line
        config["name"] = f"unit_test: {filename}"
        config["program"] = f"unit_tests/{filename}"
        config["args"] = ["0"]
    
    elif line[0] == "arg_tests":
        pass

    elif line[0] == "cfg_tests":
        pass
    

    configurations.append(config)


print(
    json.dumps(
        { 
            "version":  "0.2.0",
            "configurations": configurations 
        },
        indent=4
    )
)