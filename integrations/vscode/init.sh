#! /bin/bash


if [ ! -d ".vscode" ]; then
    mkdir ".vscode";
fi

# Generate "launch.json" file for vscode $language debugger
if [ -f ".admin_files/integrations/vscode/$language-debugger.py" ]; then
    export main_file
    export main_file_arguments
    {
        shopt -s nullglob
        for filename in stdio_tests/inputs/*.txt; do
            filename=$(basename $filename)
            echo "stdio_tests $filename"
        done
        
        for filename in unit_tests/*.py; do
            filename=$(basename $filename)
            echo "unit_tests $filename"
        done

        for filename in arg_tests/inputs/*; do
            filename=$(basename $filename)
        done
        
        for filename in cfg_tests/*; do
            filename=$(basename $filename)
        done
        # Output json doc on empty line
        echo ""
    } | python3 .admin_files/integrations/vscode/$language-debugger.py > ".vscode/launch.json"
fi

show_diff() {
    code --diff $1 $2
}

start_debugger() {
    : # TODO start vscode debugger
}
