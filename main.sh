USAGE="
Usage: grade.sh [options] [ grade-only | build | install | stream | share | clean ] 

Subcommands:
    grade-only      No interactive prompts
    build           Call build script
    install         Install assignment dependencies
    stream          Run as service over stdin/stdout
    share           Create shareable onion link
    clean           Clean tor service

Options:
    --debug            Run in debug mode for developers
    --file=<name>      Dump output to file
"

# Default terminal front end
terminal() {
    local num_tests
    local total_score
    local restart
    NULL_GLOB


    test_loop() {
        local testclass="$1"
        local testname="$2"
        local restart=1

        while true; do
            database.render $testclass $testname --header
            ${testclass}.runTest $testname --echo
            result=$?
            database.render $testclass $testname --result
            if [ "$result" == "0" ]; then
                break
            else
                restart=0
            fi

            if command -v $testclass.doAfter $testname >/dev/null; then
                if ! ${testclass}.doAfter $testname; then
                    log "ERRROR: $testclass.doAfter"
                    return 1
                    break
                fi
                keepgoing --restart-only
            else
                echo "No command to handle failing test"
                
                break
            fi
            
            
        done
        return $restart
    }

    term_init
    
    echo "testclass" >& ${TESTDATA[1]} && read -a testclasses <& ${TESTDATA[0]}
    for testclass in ${testclasses[@]}; do 
        echo "testnames $testclass" >& ${TESTDATA[1]} && read -a testnames <& ${TESTDATA[0]}
        for testname in ${testnames[@]}; do
            database.render $testclass $testname --header
            ${testclass}.runTest $testname --echo
            database.render $testclass $testname --result
        done
    done

    if ! stty size >/dev/null 2>/dev/null; then
        echo "$(python3 -c "print('=' * 120)")"
    else
        echo "$(python3 -c "print('=' * $(stty size | awk '{print $2}'))")"
    fi

    database.grade_summary
    if $GRADE_ONLY; then
        done_message
        database.grade total
        echo -e "Your grade is:\n$total" > $student_file
        if [ "$total" != 100 ]; then
            exit 1
        fi
        exit 0
    fi

    Handle_SIGINT()
    {
        clear
        while read -t 0.1 <& ${TESTDATA[0]}; do
            :
        done
        database.grade_summary
        done_message
        exit 0
    }

    trap "Handle_SIGINT" SIGINT

    echo "Press ${CYAN}Enter${RESET} for assistance" && read;
    
    for testclass in ${testclasses[@]}; do
        echo "testnames $testclass" >& ${TESTDATA[1]} && read -a testnames <& ${TESTDATA[0]}
        for testname in ${testnames[@]}; do
            if test_loop $testclass $testname; then
                break 2
            fi
        done
    done

    exit

        

    Handle_SIGINT # print summary 
    return 0
}

# Expose run command over stdin/stdout
stream() {
    while true; do
        read line
        eval cmd=($line)
        if [ "${cmd[0]}" == "run" ] \
            || [ "${cmd[0]}" == "get" ];
        then
            ("${cmd[@]}")
        else
            log "Unknown command '${cmd[0]}'"
        fi
    done
}

source .admin_files/util.sh

# Dependency handling
source .admin_files/requirements/init.sh

# IDE specific integrations
source .admin_files/integrations/init.sh

# Build sources
source .admin_files/builder/init.sh

# Shareable session link
source .admin_files/tor-init.sh

# args
eval "$(./.admin_files/docopts -A args -h "$USAGE" : "$@")"

GRADE_ONLY="${args[grade-only]}"
DEBUG="${args[--debug]}"

if "${args[stream]}"; then
    build_generic && (python3 .admin_files/tester/testrunner.py)
elif "${args[share]}"; then
    tor_init
elif "${args[clean]}"; then
    tor_stop
elif "${args[build]}"; then
    build_generic
elif "${args[install]}"; then
    install_generic
elif [ -n "${args[--file]}" ]; then
    bash grade.sh --grade-only | sed "s,\x1B\[[0-9;]*[a-zA-Z],,g" >${args[--file]}
else
    build_generic && terminal
fi