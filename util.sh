free_port () {
    local start=$1
    local lsof_result

    if [ ! "$start" ]; then
        start=10000;
    fi;

    while true; do
        lsof_result=$(lsof -i:$start)
        if [ "$?" != 0 ]; then
            echo $start
            return
        else
            ((start=start + 1))
        fi
    done
}

kill_listener() {
    local portnum=$1
    local listener=$(lsof -i:$portnum | awk 'NR>1{ print $2 }')
    kill -15 $listener 2>/dev/null
}


add_path() {
    # Add dirname to path if it doesn't exist already
    local dirname="$1"
    if [ -d "$dirname" ]; then
        mkdir -p "$dirname"
    fi
    
    if [[ ! $(printenv | grep PATH | grep "$dirname") ]]; then
        echo ""                                     >> $HOME/.profile
        echo "if [ -d "$dirname" ]; then"           >> $HOME/.profile
        echo "  PATH=\"$dirname:$PATH\""            >> $HOME/.profile
        echo "fi"                                   >> $HOME/.profile
        echo ""                                     >> $HOME/.profile
        # Will this interfere with other shells?
        export PATH="$dirname:$PATH"
    fi
}

NULL_GLOB() {
    # To produce null strings when no match
    shopt -s nullglob
}

section() {
    echo -e "=============================================================="
}

subsection() {
    echo -e "--------------------------------------------------------------"
}

keepgoing() {

    reset_cursor_pos() {
        echo -en "\033[1K\033[1F\033[K"
    }

    local readval="?"
    if $DEBUG; then
        if [[ "$1" == "--no-restart" ]]; then
            read -n1 -p "Press ${CYAN}Enter${RESET} to continue: " readval
            reset_cursor_pos
            return 0
        elif [[ "$1" == "--restart-only" ]]; then
            read -n1 -p "Press ${CYAN}Enter${RESET} to restart: " readval 
            reset_cursor_pos
            return 1
        else
            while true; do
                read -n1 -p "Press ${CYAN}R${RESET} to restart or ${CYAN}Enter${RESET} to continue: " readval
                if [ "$readval" = "r" ] \
                    || [ "$readval" = "R" ];
                then
                    echo ""
                    reset_cursor_pos
                    return 1
                elif [ "$readval" ]; then
                    echo ""
                    reset_cursor_pos
                    continue
                else
                    reset_cursor_pos
                    return 0
                fi
            done
        fi
    fi
}

check_hashes() {
    # This should be run before any file students could potentially edit.
    # Assumes you've addded the relevant files to hash_gen.sh
    # usage: check_hashes
    # () creates a subshell, and so no need to cd back
    (
        cd .admin_files
        bash hash_gen.sh
        # cd ..
    )

    if ! diff .admin_files/hashes.txt .admin_files/grader_hashes.txt &>/dev/null; then
        if ! $DEBUG; then
            hash_alert
            exit 1
        fi
    fi
}

welcome_message() {
    echo -e "Welcome to grade.sh!\nThis script will mommy you through completing this programming assignment.\n"
    echo -e "    $MAGENTA""Magenta""$RESET" is bash/shell commands.
    echo -e "    $GREEN""Green""$RESET" is used to indicate a pass.
    echo -e "    $RED""Red""$RESET" is used to indicate a fail.
    echo -e "    $ORANGE""Orange""$RESET" marks the text of a header for a block of tests of a sinle type.
    echo -e "    $CYAN""Cyan""$RESET" draws your attention to something you have to do.
    echo -e "\nActually$CYAN read ALL the output generated by this script!\n$RESET"
    echo -e "\nFor this particular assignment:"
    if [ "$fuzzy_partial_credit" = false ]; then
        echo "* Diffs are computed rigidly -- no partial credit!"
    else
        echo "* Diffs are computed fuzzy -- with partial credit!"
    fi
    echo "* Each launch of your $main_file code must run in under $process_timeout seconds"
}

prompt_run_mode() {
    section
    echo "Which of the following modes do you want to run in?"
    echo -e "    1) debug+grade mode (d)"
    echo -e "    2) grade only mode (g)"
    echo ""
    
    while true; do
        read -n1 -p "Press ${CYAN}D${RESET} or ${CYAN}G${RESET}: " char
        if [ "$char" == "G" ] || [ "$char" == "g" ]; then
            DEBUG=false
            break
        elif [ "$char" == "D" ] || [ "$char" == "d" ]; then
            DEBUG=true
            break
        else
            if [ "$char" ]; then
                echo ""
            fi
            echo -en "\033[1K\033[1F\033[K"
        fi
    done
    echo ""

}

timer_start() {
    TIMER_START=$(date +%s.%N)
}

timer_stop() {
    D_TIME=$(echo "print($(date +%s.%N) - $TIMER_START)" | python3)
}

term_init() {
    # Clears the screen
    tput reset
    BLACK=$(tput setaf 0)
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    YELLOW=$(tput setaf 3)
    BLUE=$(tput setaf 4)
    MAGENTA=$(tput setaf 5)
    CYAN=$(tput setaf 6)
    WHITE=$(tput setaf 7)
    ORANGE=$(tput setaf 202)
    RESET=$(tput sgr0)
}

header() {
    local testclass="$1"
    local testname="$2"
    local cmd="$3"
    section
    echo "${YELLOW}${testclass} - ${testname}${RESET}"
    subsection
    echo "${MAGENTA}$ ${cmd}${RESET}"
    subsection
}

print_score() {
    local score="$1"
    if (( score < 70 )); then
        echo "${RED}${score}${RESET}"
    elif ((score < 90)); then
        echo "${YELLOW}${score}${RESET}"
    elif ((score <= 100)); then
        echo "${GREEN}${score}${RESET}"
    fi
}

done_message() {
    echo -e "\n\033[36mThe last step is to push your changes to git-classes:"
    echo
    echo -e "        $(git config --get remote.origin.url)/-/pipelines"
    echo
    echo -e "Make sure the CI passes, turns green, and you inspect the job itself for your grade."
    echo -e "Actually look at the Gitlab CI details to see what we think your current grade is."
    echo -e "Unless you see the output and numerical grade below on Gitlab, do not assume you are done!\n\033[0m"
}

log() {
    echo "$@" >&2
}

build()
{
    if [ ! -z "$build_command" ]; then
        $build_command
    fi
}
